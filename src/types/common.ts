export type IndexedObject<V = any> = { [k: string]: V };

export type TAction<T> = {
  type: string;
  payload: IndexedObject<T>;
};

export type Locale = { code: string; name: string; bidi: boolean };

export type TOption<T = string | number> = {
  label: string;
  value?: T;
};

export type TUser = {
  id_user: number;
  id_employee: string;
  username: string;
  fullname: string;
  avatar: string;
  role: "PHONGBAN" | "HCNS" | "USER";
  department: {
    id_department: number;
    name_department: string;
    descrtiption: string;
  };
  vehicle: Object;
}

export type TCar = {
  carId: string;
  carName: string;
  carStatus: boolean;
  carLicensePlate: string;
  carImage: string | null;
  driverName: string;
  driverId: string;
  seats: number;
}
