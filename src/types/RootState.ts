// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

/*
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
*/
import { Province } from "store/reducer/locationReducer"
import { IndexedObject, TCar, TUser } from "./common"
export interface RootState {
  auth: {
    currentUser: {
      accessToken: string,
      refreshToken: string,
      user: TUser

    },
    authenticated: boolean,
    // loginError: string
  },
  location: {
    provinces: Array<Province>
  },
  car: {
    cars: Array<any>,
    carDetail: IndexedObject
  },
  user: {},
  booking: {
    listWait: Array<any>,
    listWaitInCity: Array<any>,
    listWaitOutCity: Array<any>,
    listCancelled: Array<any>,
    listWaitToCancel: Array<any>,
    listApproved: Array<any>,
    listRejected: Array<any>,
    bookingInfo: IndexedObject,
  },
  travel: {
    incomingTravel: Array<any>,
    completedTravel: Array<any>,
  }
}
