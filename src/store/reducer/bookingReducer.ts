import { createAction, createReducer } from '@reduxjs/toolkit';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { RootState } from 'types/RootState';
import { BookingDetail } from 'app/pages/BookCarDetailPage/BookCarDetailPage';
import bookingApi from 'app/axios/api/bookingApi';
import { CancelBookingProps } from 'app/components/Common/BookingItem/BookingItem';
import { IndexedObject } from 'types/common';

export const bookingRequest = createAsyncThunk('booking/request',
    async (data: BookingDetail, { rejectWithValue }) => {
        console.log(data);

        try {
            const request = await bookingApi.createBookingRequest(data);
            console.log(request.data);
            return request.data
        } catch (error: any) {
            console.log(error);
            return error.response.data
        }
    }
);

export const getUnapprovedBooking = createAsyncThunk('booking/listWait', async (_, { rejectWithValue }) => {
    try {
        const request = await bookingApi.getUnapprovedRequest();
        // console.log(request.data);
        return request.data
    } catch (error: any) {
        console.log(error);
        rejectWithValue(error.response.data);
        return error.response.data
    }
});

export const getApprovedBooking = createAsyncThunk('booking/listApproved', async (_, { rejectWithValue }) => {
    try {
        const request = await bookingApi.getApprovedRequest();
        // console.log(request.data);
        return request.data
    } catch (error: any) {
        console.log(error);
        rejectWithValue(error.response.data);
        return error.response.data
    }
});

export const getBookingRequestIncity = createAsyncThunk('booking/listWaitInCity', async (_, { rejectWithValue }) => {
    try {
        const request = await bookingApi.getRequestInCity();
        // console.log(request.data);
        return request.data
    } catch (error: any) {
        console.log(error);
        rejectWithValue(error.response.data);
        return error.response.data
    }
});

export const getBookingRequestOutcity = createAsyncThunk('booking/listWaitOutCity', async (_, { rejectWithValue }) => {
    try {
        const request = await bookingApi.getRequestOutsideCity();
        // console.log(request.data);
        return request.data
    } catch (error: any) {
        console.log(error);
        rejectWithValue(error.response.data);
        return error.response.data
    }
});

export const cancelBooking = createAsyncThunk('booking/cancel',
    async (data: CancelBookingProps, { rejectWithValue }) => {
        try {
            const result = await bookingApi.cancelBookingRequest(data);
            // console.log(request.data);
            return result.data;
        } catch (error: any) {
            console.log(error);
            rejectWithValue(error.response.data);
            return error.response.data;
        }
    }
);

export const getCanceledBooking = createAsyncThunk('booking/listCancelled',
    async (_, { rejectWithValue }) => {
        try {
            const result = await bookingApi.getCanceledRequest();
            // console.log(request.data);
            return result.data;
        } catch (error: any) {
            console.log(error);
            rejectWithValue(error.response.data);
            return error.response.data;
        }
    }
);

export const getBookingInfomation = createAsyncThunk('booking/getBookingInfo',
    async (id: number, { rejectWithValue }) => {
        try {
            const result = await bookingApi.getBookingInfomation(id);
            // console.log(request.data);
            return result.data;
        } catch (error: any) {
            console.log(error);
            rejectWithValue(error.response.data);
            return error.response.data;
        }
    }
);

export const approveBooking = createAsyncThunk('booking/approve',
    async (id: number, { rejectWithValue }) => {
        try {
            const result = await bookingApi.approveBookingRequest(id);
            // console.log(request.data);
            return result.data;
        } catch (error: any) {
            console.log(error);
            rejectWithValue(error.response.data);
            return error.response.data;
        }
    }
);

const initialState = {
    listWait: [] as any,
    listWaitInCity: [] as any,
    listWaitOutCity: [] as any,
    listCancelled: [] as any,
    listWaitToCancel: [] as any,
    listApproved: [] as any,
    listRejected: [] as any,
    bookingInfo: {} as IndexedObject,

}

const booking = createSlice({
    name: 'booking',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(bookingRequest.pending, (state, action) => {
                console.log('booking request pending ...');

            })
            .addCase(bookingRequest.fulfilled, (state, action) => {
                console.log(action.payload);
            })
            .addCase(bookingRequest.rejected, (state, action) => {
                console.log('booking request rejected ...', action.payload);

            })
            .addCase(getUnapprovedBooking.pending, (state, action) => {
                console.log('booking request pending ...');

            })
            .addCase(getUnapprovedBooking.fulfilled, (state, action) => {
                // console.log(action.payload);
                if (!action.payload.statusCode) {

                    state.listWait = [...action.payload];
                }
            })
            .addCase(getUnapprovedBooking.rejected, (state, action) => {
                console.log('booking request rejected ...', action.payload);

            })
            .addCase(getApprovedBooking.pending, (state, action) => {
                console.log('booking request pending ...');

            })
            .addCase(getApprovedBooking.fulfilled, (state, action) => {
                // console.log(action.payload);
                if (!action.payload.statusCode) {

                    state.listApproved = [...action.payload];
                }
            })
            .addCase(getApprovedBooking.rejected, (state, action) => {
                console.log('booking request rejected ...', action.payload);

            })
            .addCase(cancelBooking.pending, (state, action) => {
                console.log('booking request pending ...');

            })
            .addCase(cancelBooking.fulfilled, (state, action) => {
                console.log(action.payload);

            })
            .addCase(cancelBooking.rejected, (state, action) => {
                console.log('booking request rejected ...', action.payload);

            })

            .addCase(getBookingRequestIncity.pending, (state, action) => {
                console.log('booking request pending ...');

            })
            .addCase(getBookingRequestIncity.fulfilled, (state, action) => {
                // console.log(action.payload);
                if (!action.payload.statusCode) {

                    state.listWaitInCity = [...action.payload];
                }
            })
            .addCase(getBookingRequestIncity.rejected, (state, action) => {
                console.log('booking request rejected ...', action.payload);

            })
            .addCase(getBookingRequestOutcity.pending, (state, action) => {
                console.log('booking request pending ...');

            })
            .addCase(getBookingRequestOutcity.fulfilled, (state, action) => {
                // console.log(action.payload);
                if (!action.payload.statusCode) {

                    state.listWaitInCity = [...action.payload];
                }
            })
            .addCase(getBookingRequestOutcity.rejected, (state, action) => {
                console.log('booking request rejected ...', action.payload);

            })
            .addCase(getCanceledBooking.pending, (state, action) => {
                console.log('booking request pending ...');

            })
            .addCase(getCanceledBooking.fulfilled, (state, action) => {
                // console.log(action.payload);
                if (!action.payload.statusCode) {

                    state.listCancelled = [...action.payload];
                }
            })
            .addCase(getCanceledBooking.rejected, (state, action) => {
                console.log('booking request rejected ...', action.payload);

            })

            .addCase(getBookingInfomation.pending, (state, action) => {
                console.log('booking request pending ...');

            })
            .addCase(getBookingInfomation.fulfilled, (state, action) => {
                // console.log(action.payload);
                if (!action.payload.statusCode) {

                    state.bookingInfo = { ...action.payload };
                }
            })
            .addCase(getBookingInfomation.rejected, (state, action) => {
                console.log('booking request rejected ...', action.payload);

            })
            .addCase(approveBooking.pending, (state, action) => {
                console.log('approving request pending ...');

            })
            .addCase(approveBooking.fulfilled, (state, action) => {
                console.log(action.payload);
            })
            .addCase(approveBooking.rejected, (state, action) => {
                console.log('approving request rejected ...', action.payload);

            })
    },
}
);


export default booking.reducer;
export const listWait = (state: RootState) => state.booking.listWait;
export const listWaitInCity = (state: RootState) => state.booking.listWaitInCity;
