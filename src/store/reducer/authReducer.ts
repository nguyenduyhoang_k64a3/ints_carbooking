import { createAction, createReducer } from '@reduxjs/toolkit';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { TUser } from 'types/common';
import authApi from 'app/axios/api/authApi';
import { saveRefreshToken, saveToken, saveUser } from 'app/helpers/localStorage';
import { RootState } from 'types/RootState';
// import { useHistory } from 'react-router-dom';


export const login = createAsyncThunk('auth/login',
    async (data: { username: string; password: string }, { rejectWithValue }) => {
        try {
            const user = await authApi.login(data);
            console.log(user.data);
            return user.data;
        } catch (error: any) {
            console.log(error.response.data);
            return rejectWithValue(error.response.data)

        }
    });
const refreshToken = createAsyncThunk('auth/refreshToken',
    async (data: { refreshToken: string }, { rejectWithValue }) => {
        try {
            const user = await authApi.refreshToken(data);
            console.log(user.data);
            return user.data;
        } catch (error: any) {
            console.log(error.response.data);
            return rejectWithValue(error.response.data)

        }
    });



const initialState = {
    currentUser: {
        accessToken: '',
        refreshToken: '',
        user: {} as TUser

    },
    authenticated: false,
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(login.pending, (state, action) => {
                console.log('login pending ...');

            })
            .addCase(login.fulfilled, (state, action) => {
                // state.currentUser = action.payload;
                if (action.payload.accessToken) {
                    const {
                        accessToken, refreshToken, user
                    } = action.payload
                    state.currentUser.accessToken = accessToken;
                    state.currentUser.refreshToken = refreshToken;
                    state.currentUser.user = user;
                    state.authenticated = true;
                } else if (action.payload.error) {
                    console.log(action.payload.message);

                }



            })
            .addCase(login.rejected, (state, action) => {
                console.log('login rejected ...', action.payload);

            })

    },
}
);


export default authSlice.reducer;
export const authenticated = (state: RootState) => state.auth.authenticated;
