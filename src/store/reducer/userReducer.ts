import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { TUser } from 'types/common';
import userApi from 'app/axios/api/userApi';

export const updateInfo = createAsyncThunk('user/updateInfo',
    async (data: { fullname: string; department: string }) => {
        try {
            const user = await userApi.updateInfo(data);
            // console.log(user.data);
            return user.data;
        } catch (error: any) {
            console.log(error);
            return error.response.data;
        }
    },
);

export const updateAvatar = createAsyncThunk('user/updateAvatar',
    async (data: FormData) => {
        console.log('avatar day ne ', data.get('avatar'));
        try {
            const user = await userApi.updateAvatar(data);
            return user.data;
        } catch (error: any) {
            console.log(error);
            return error.response.data;
        }
    }
);

export const changePassword = createAsyncThunk('user/changePassword',
    async (data: { passwordOld: string; passwordNew: string; repeatPassword: string }) => {
        try {
            const res = await userApi.changePassword(data);
            console.log(res.data);
            return res.data;
        } catch (error: any) {
            console.log(error);
            return error.response.data;
        }
    },
);

const initialState = {
    user: {} as TUser,
};

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(updateInfo.pending, (state, action) => {
                console.log('update info pending ...');
            })
            .addCase(updateInfo.fulfilled, (state, action) => {
                console.log('updated sucessfully', action.payload);
            })
            .addCase(updateInfo.rejected, (state, action) => {
                console.log('update info rejected ...', action.payload);
            })
            .addCase(updateAvatar.pending, (state, action) => {
                console.log('update Avatar pending ...');
            })
            .addCase(updateAvatar.fulfilled, (state, action) => {
                // state.user = action.payload;
                // saveUser(action.payload)
                console.log('updated sucessfully', action.payload);
            })
            .addCase(updateAvatar.rejected, (state, action) => {
                console.log('update Avatar rejected ...', action.payload);
            })
            .addCase(changePassword.pending, (state, action) => {
                console.log('update Avatar pending ...');
            })
            .addCase(changePassword.fulfilled, (state, action) => {
                console.log('updated sucessfully', action.payload);
            })
            .addCase(changePassword.rejected, (state, action) => {
                console.log('update Avatar rejected ...', action.payload);
            });
    },
});
export default userSlice.reducer;
