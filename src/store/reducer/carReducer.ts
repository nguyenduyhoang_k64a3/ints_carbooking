import { createAction, createReducer } from '@reduxjs/toolkit';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { RootState } from 'types/RootState';
import carApi from 'app/axios/api/carApi';
import { TCar } from 'types/common';

export const getAllCar = createAsyncThunk('car/getAll',
    async (_, { rejectWithValue }) => {
        try {
            const car = await carApi.getAllCars();
            console.log(car.data);
            return car.data;
        } catch (error: any) {
            console.log(error.response.data);
            return rejectWithValue(error.response.data);
        }
    });

export const getCarById = createAsyncThunk('car/getById', async (id: string, { rejectWithValue }) => {
    try {
        const car = await carApi.getCarById(id);
        console.log(car.data);
        return car.data;
    } catch (error: any) {
        console.log(error);
        return error.response.data;

    }
});

export const search = createAsyncThunk('car/search', async (search: string, { rejectWithValue }) => {
    try {
        const cars = await carApi.search(search);
        console.log(cars.data);
        return cars.data;
    } catch (error: any) {
        console.log(error);
        return error.response.data;
    }
});

const initialState = {
    cars: [] as any,
    carDetail: {} as any
};

const carSlice = createSlice({
    name: 'car',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getAllCar.pending, (state, action) => {
                console.log('get cars pending ...');
            })
            .addCase(getAllCar.fulfilled, (state, action) => {
                state.cars = action.payload;
            })
            .addCase(getAllCar.rejected, (state, action) => {
                console.log('get cars rejected ...', action.payload);
            })
            .addCase(getCarById.pending, (state, action) => {
                console.log('get cars pending ...');
            })
            .addCase(getCarById.fulfilled, (state, action) => {
                console.log(action.payload);
                state.carDetail = {
                    driverName: action.payload.fullname,
                    driverId: action.payload.id_user,
                    carLicensePlate: action.payload.vehicle.license_plate,
                    carId: action.payload.id_user,
                    carImage: action.payload.vehicle.image_vehicle,
                    carName: action.payload.vehicle.type,
                    carStatus: action.payload.vehicle.status_vehicle,
                    department: action.payload.role,
                    driverPhone: action.payload.username,
                    seats: action.payload.vehicle.seats,
                };
                console.log('get car by id successfully...', action.payload);
            })
            .addCase(getCarById.rejected, (state, action) => {
                console.log('get cars rejected ...', action.payload);
            })
            .addCase(search.pending, (state, action) => {
                console.log('search cars pending ...');
            })
            .addCase(search.fulfilled, (state, action) => {
                state.cars = action.payload;
            })
            .addCase(search.rejected, (state, action) => {
                console.log('search cars rejected ...', action.payload);
            });
    },
});

export default carSlice.reducer;
export const cars = (state: RootState) => state.car.cars;
export const car = (state: RootState) => state.car.carDetail;
