import { createAction, createReducer } from '@reduxjs/toolkit';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { TUser } from 'types/common';
import authApi from 'app/axios/api/authApi';
import { saveRefreshToken, saveToken, saveUser } from 'app/helpers/localStorage';
import { RootState } from 'types/RootState';
import locationApi from 'app/axios/api/locationApi';
// import { useHistory } from 'react-router-dom';

type District = {

}
export type Province = {
    name: string;
    code: number;
    divisionType: string;
    phoneCode: number;
    districts: Array<any>;
}

export const getProvinces = createAsyncThunk('location/get_provinces',
    async () => {
        try {
            const provinceList = await locationApi.getAllProvinces();

            if (provinceList.data.errors) {
                console.log('error getting provinces');

            }
            console.log(provinceList.data);
            return provinceList.data
        } catch (error) {
            console.log(error);

        }
        // console.log('aaaa');


    });

const initialState = {
    provinces: [] as Array<Province>
}

const locationSlice = createSlice({
    name: 'location',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getProvinces.pending, (state, action) => {
                console.log('get Provinces pending ...');

            })
            .addCase(getProvinces.fulfilled, (state, action) => {
                console.log('Provinces list: ', action.payload);
                // state.currentUser = action.payload;
                state.provinces = action.payload;



            })
            .addCase(getProvinces.rejected, (state, action) => {
                console.log('get provinces rejected ...', action.payload);

            })

    },
}
);


export default locationSlice.reducer;

