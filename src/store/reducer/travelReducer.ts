import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import travelApi from 'app/axios/api/travelApi';

export const getIncomingTravel = createAsyncThunk('travel/getIncomingTravel',
    async (_, { rejectWithValue }) => {
        try {
            const listIncomingTravel = await travelApi.getIncomingTravel();
            console.log(listIncomingTravel.data);
            return listIncomingTravel.data;
        } catch (error: any) {
            console.log(error.response.data);
            return rejectWithValue(error.response.data)

        }
    });

export const getCompletedTravel = createAsyncThunk('travel/getCompletedTravel',
    async (_, { rejectWithValue }) => {
        try {
            const listIncomingTravel = await travelApi.getCompledtedTravel();
            console.log(listIncomingTravel.data);
            return listIncomingTravel.data;
        } catch (error: any) {
            console.log(error.response.data);
            return rejectWithValue(error.response.data)

        }
    });

const initialState = {
    incomingTravel: [] as any,
    completedTravel: [] as any,
}

const travelReducer = createSlice({
    name: 'travel',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getIncomingTravel.pending, (state, action) => {
                console.log('getIncomingTravel pending ...');

            })
            .addCase(getIncomingTravel.fulfilled, (state, action) => {
                state.incomingTravel = [...action.payload]
            })
            .addCase(getIncomingTravel.rejected, (state, action) => {
                console.log('getIncomingTravel rejected ...', action.payload);

            })
            .addCase(getCompletedTravel.pending, (state, action) => {
                console.log('getCompletedTravel pending ...');

            })
            .addCase(getCompletedTravel.fulfilled, (state, action) => {
                state.completedTravel = [...action.payload]
            })
            .addCase(getCompletedTravel.rejected, (state, action) => {
                console.log('getCompletedTravel rejected ...', action.payload);

            })

    },
}
);

export default travelReducer.reducer;
