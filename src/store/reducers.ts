/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from '@reduxjs/toolkit';

import { InjectedReducersType } from 'utils/types/injector-typings';
import authReducer from './reducer/authReducer';
import locationReducer from './reducer/locationReducer';
import carReducer from './reducer/carReducer';
import bookingReducer from './reducer/bookingReducer';
import travelReducer from './reducer/travelReducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export const rootReducers = {
  auth: authReducer,
  location: locationReducer,
  car: carReducer,
  booking: bookingReducer,
  travel: travelReducer,
};

export function createReducer(injectedReducers: InjectedReducersType = {}) {
  // Initially we don't have any injectedReducers, so returning identity function to avoid the error
  if (Object.keys(injectedReducers).length === 0) {
    return (state: any) => state;
  } else {
    return combineReducers({
      ...injectedReducers,
    });
  }
}
