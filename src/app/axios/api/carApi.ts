import { getTokenFromLocalStorage } from "app/helpers/localStorage";
import { axiosClient } from "../axiosClient";

const apiUrl = process.env.REACT_APP_API_URL
const carApi = {
    getCarById: async (id: string) => {
        return await axiosClient.get(`${apiUrl}/api/v2/users?id=${id}`,)
    },

    getAllCars: async () => {
        return await axiosClient.get(`${apiUrl}/api/v2/users/listUser`)
    },
    search: async (search: string) => {
        return await axiosClient.post(`${apiUrl}/api/v2/users/search`,{search})
    },

};

export default carApi;
