import { getTokenFromLocalStorage } from "app/helpers/localStorage";
import { axiosClient } from "../axiosClient";
import { BookingDetail } from "app/pages/BookCarDetailPage/BookCarDetailPage";
import { CancelBookingProps } from "app/components/Common/BookingItem/BookingItem";

const apiUrl = process.env.REACT_APP_API_URL
const bookingApi = {
    createBookingRequest: async (data: BookingDetail) => {
        return await axiosClient.post(`${apiUrl}/api/v2/booking-details/create?id_driver=${data.driver_id}`, data);
    },
    getUnapprovedRequest: async () => {
        return await axiosClient.get(`${apiUrl}/api/v2/booking-details/listWaitAccess`);
    },
    getApprovedRequest: async () => {
        return await axiosClient.get(`${apiUrl}/api/v2/booking-details/list/accessBooking`);
    },
    getCanceledRequest: async () => {
        return await axiosClient.get(`${apiUrl}/api/v2/reason/list/CancelPb`);
    },

    // role HCNS
    getRequestInCity: async () => {
        return await axiosClient.get(`${apiUrl}/api/v2/booking-details/list/incity`);
    },
    // role HCNS
    approveBookingRequest: async (id: number) => {
        return await axiosClient.patch(`${apiUrl}/api/v2/booking-details/updateAccess?id_booking=${id}`);
    },
    // role BGD
    getRequestOutsideCity: async () => {
        return await axiosClient.get(`${apiUrl}/api/v2/booking-details/list/outcity`);
    },
    getBookingInfomation: async (id_booking: number) => {
        return await axiosClient.get(`${apiUrl}/api/v2/reason/detail/Cancel?id_booking=${id_booking}`);
    },

    // Role PHONGBAN
    cancelBookingRequest: async (data: CancelBookingProps) => {
        return await axiosClient.patch(`${apiUrl}/api/v2/booking-details/updateCancel?id_booking=${data.id_booking}`, data);
    },
};

export default bookingApi;
