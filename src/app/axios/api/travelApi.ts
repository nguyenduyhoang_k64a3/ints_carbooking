import { axiosClient } from "../axiosClient";

const apiUrl = process.env.REACT_APP_API_URL
const travelApi = {
    getIncomingTravel: async () => {
        return await axiosClient.get(`${apiUrl}/api/v2/booking-details/list/accessFinishBooking?status=Ac`);
    },
    getCompledtedTravel: async () => {
        return await axiosClient.get(`${apiUrl}/api/v2/booking-details/list/accessFinishBooking?status=Fi`);
    },
    
};

export default travelApi;
