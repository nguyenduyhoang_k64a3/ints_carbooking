import { UploadAvatar } from "app/pages/UserInfoPage/UserInfoEditPage";
import { axiosClient } from "../axiosClient";

const apiUrl = process.env.REACT_APP_API_URL;
const userApi = {
    updateInfo: async (data: { fullname: string }) => {
        return await axiosClient.patch(`${apiUrl}/api/v2/users/updateInfor`, data);
    },
    updateAvatar: async (data: FormData) => {
        console.log(data);
        return await axiosClient.patch(`${apiUrl}/api/v2/users/uploadAvatar`, data, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        });
    },
    changePassword: async (data: { passwordOld: string; passwordNew: string; repeatPassword: string }) => {
        return await axiosClient.patch(`${apiUrl}/api/v2/users/updatePassword`, data);
    },

};

export default userApi;
