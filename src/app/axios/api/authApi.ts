import axios from "axios";
import { axiosClient } from "../axiosClient";
const apiUrl = process.env.REACT_APP_API_URL

const authApi = {
    login: async (data: { username: string; password: string }) => {
        return await axios.post(`${apiUrl}/api/v2/users/login`, data)
    },
    refreshToken: async (data: { refreshToken: string }) => {
        return await axios.post(`${apiUrl}/api/v2/auth/refresh`, data)
    },
};

export default authApi;
