import { axiosClient } from "../axiosClient";

const locationApi = {
    getAllProvinces: async () => {
        return await axiosClient.get(`https://provinces.open-api.vn/api/p/`,)
    },

    getAllDistricts: async () => {
        return await axiosClient.get(`https://provinces.open-api.vn/api/d/`,)
    },


};

export default locationApi;
