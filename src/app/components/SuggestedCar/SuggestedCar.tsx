import React, { useEffect } from 'react';
import CarItemCard from '../Common/CarItemCard/CarItemCard';
import { useDispatch, useSelector } from 'react-redux';
import { getAllCar, cars } from 'store/reducer/carReducer';
import { Link, useHistory } from 'react-router-dom';
import { getBookingRequestIncity, getBookingRequestOutcity, listWaitInCity } from 'store/reducer/bookingReducer';
import BookingItem from '../Common/BookingItem/BookingItem';
import { RootState } from 'types/RootState';

type Props = {
  role: string;
  title: string;
}
const SuggestedCar: React.FC<Props> = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchCar = async () => {
      const response: any = await dispatch(getAllCar())
      console.log(response);
      if (response.payload.statusCode === 401) {
        history.push("/login");
      } else if (response.payload.statusCode < 200 || response.payload.statusCode >= 300) {
        console.log(response.payload.message);

      }
    };
    const fetchBookingRequestIncity = async () => {
      const result: any = await dispatch(getBookingRequestIncity());
      console.log(result);
      if (result.payload.statusCode === 401) {
        history.push("/login");
      } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
        console.log(result.payload.message);

      }
    };
    const fetchBookingRequestOutcity = async () => {
      const result: any = await dispatch(getBookingRequestOutcity());
      console.log(result);
      if (result.payload.statusCode === 401) {
        history.push("/login");
      } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
        console.log(result.payload.message);

      }
    };
    props.role === 'PHONGBAN' && fetchCar();
    props.role === 'HANHCHINH' && fetchBookingRequestIncity();
    props.role === 'BGD' && fetchBookingRequestIncity();
  }, []);
  const carList = useSelector(cars);
  const requestListIncity = useSelector(listWaitInCity);
  const requestListOutcity = useSelector((state: RootState) => state.booking.listWaitOutCity);
  console.log(carList);

  return (
    <div className='w-100 py-4'>
      <div
        className='w-100 mx-3 mb-3'

      >
        <h2 className='fw-semibold text-dark mb-2'>Gợi ý</h2>
        <div className='w-25'
          style={{ borderBottom: '2px solid #5470f2' }}>

        </div>
      </div>
      <div className="w-100 d-flex flex-row justify-content-between flex-column mb-4">
        {props.role === 'PHONGBAN' && (carList.length > 0 ?
          carList.map((car, index) => {
            console.log(car.vehicle.license_plate);

            return (
              <>
                <CarItemCard
                  key={index}
                  driverName={car.fullname}
                  carLicensePlate={car.vehicle.license_plate}
                  carId={car.vehicle.id_vehicle}
                  carImage={car.vehicle.image_vehicle}
                  carName={car.vehicle.type}
                  carStatus={car.vehicle.status_vehicle}
                  seats={car.vehicle.seat}
                  driverId={car.id_user}
                />
              </>
            )
          }) : <p className='px-3'>Không tìm thấy xe nào.</p>)
        }
        <div className='px-3'>
          {props.role === 'HANHCHINH' && (requestListIncity.length > 0
            ?
            [...requestListIncity, ...requestListOutcity].filter((item, index) => {
              return index < 5;
            }).map((item, index) => {
              return (
                <BookingItem
                  bookingId={item.id_booking}
                  key={index}
                  carImage={item.driver.vehicle.image_vehicle}
                  carName={item.reason_booking}
                  startDate={`${new Date(item.date_start).toLocaleDateString()} ${item.time_start}`}
                  endDate={`${new Date(item.date_end).toLocaleDateString()} ${item.time_end}`}
                />
              )
            })
            : <p className='px-3'>Không tìm thấy yêu cầu chờ duyệt nào</p>)
          }
        </div>
      </div>
    </div >

  )
}

export default SuggestedCar