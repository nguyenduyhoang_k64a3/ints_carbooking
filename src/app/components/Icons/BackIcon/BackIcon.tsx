import c from 'clsx';
import React, { FC } from 'react';
import '../Icons.scss';
import IconProp from '../IconProps'


const BackIcon: FC<IconProp> = (props) => {
  const { className, size, fontWeight } = props;
  return (
    <div className={c('icon-component', `size${size}`, className)}>
      <svg width={size} height={size} xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={`${fontWeight}`} stroke="currentColor">
        <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />
      </svg>

    </div>
  );
};

export default BackIcon;
