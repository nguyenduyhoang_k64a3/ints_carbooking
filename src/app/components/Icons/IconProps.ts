export default interface Props {
    className?: string;
    size?: number;
    fontWeight?: number;
    color?: string;
}