import c from 'clsx';
import React, { FC } from 'react';
import '../Icons.scss';
import IconProp from '../IconProps'


const AddIcon: FC<IconProp> = (props) => {
    const { className, size, fontWeight, color } = props;
    return (
        <div className={c('icon-component', `size${size}`, className)}>
            <svg width={size} height={size} viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" strokeWidth={fontWeight} stroke={color ? color : '#000'}>
                <path fill-rule="evenodd" d="M12 3.75a.75.75 0 01.75.75v6.75h6.75a.75.75 0 010 1.5h-6.75v6.75a.75.75 0 01-1.5 0v-6.75H4.5a.75.75 0 010-1.5h6.75V4.5a.75.75 0 01.75-.75z" clip-rule="evenodd" />
            </svg>

        </div>
    );
};

export default AddIcon;
