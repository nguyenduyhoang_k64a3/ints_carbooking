import c from 'clsx';
import React, { FC } from 'react';
import '../Icons.scss';
import IconProp from '../IconProps'


const LocationIcon: FC<IconProp> = (props) => {
  const { className, size, fontWeight } = props;
  return (
    <div className={c('icon-component', `size${size}`, className)}>
      <svg width={size} height={size} viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg" strokeWidth={fontWeight}>
        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.0193 22.8079V9.77548H0L22.8079 0L13.0193 22.8079Z" fill="#5470F2" />
      </svg>


    </div>
  );
};

export default LocationIcon;
