import React from 'react';
import { EnvironmentOutlined, FileDoneOutlined, HistoryOutlined, SettingOutlined, WarningOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';

type Props = {
  title: string;
  role: string;
}
const Management: React.FC<Props> = (props) => {
  return (
    <div className='w-100 py-4'>
      <div
        className='w-full mx-3 mb-3'

      >
        <h2 className='fw-semibold text-dark mb-2'>{props.title}</h2>
        <div className='w-25'
          style={{ borderBottom: '2px solid #5470f2' }}>

        </div>
      </div>
      <div className="w-100 d-flex flex-row justify-content-between">
        {props.role === 'PHONGBAN' &&
          <>
            <Link to={'/travel-history'} className="management-item">
              <div>
                <p className='text-white fw-semibold'>Lịch sử chuyến đi</p>
                <div className='bg-white rounded-circle d-flex align-items-center justify-content-center' style={{ width: 32, height: 32, color: '#5470f2' }}>
                  <EnvironmentOutlined rev={false} style={{ fontSize: 20, fontWeight: '600' }} />
                </div>
              </div>
            </Link>
            <Link to={'/booking-history'} className="management-item">
              <div>
                <p className='text-white fw-semibold'>
                  Lịch sử đặt xe
                </p>
                <div className='bg-white rounded-circle d-flex align-items-center justify-content-center' style={{ width: 32, height: 32, color: '#5470f2' }}>
                  <HistoryOutlined rev={false} style={{ fontSize: 20, fontWeight: '600' }} />
                </div>

              </div>
            </Link>
          </>
        }
        {props.role === 'TAIXE' &&
          <>
            <Link to={'/car-status'} className="management-item">
              <div>
                <p className='text-white fw-semibold'>Lịch sử chuyến đi</p>
                <div className='bg-white rounded-circle d-flex align-items-center justify-content-center' style={{ width: 32, height: 32, color: '#5470f2' }}>
                  <SettingOutlined rev={false} style={{ fontSize: 20, fontWeight: '600' }} />
                </div>
              </div>
            </Link>
            <Link to={'/travel-history'} className="management-item">
              <div>
                <p className='text-white fw-semibold'>
                  Lịch sử chuyến đi
                </p>
                <div className='bg-white rounded-circle d-flex align-items-center justify-content-center' style={{ width: 32, height: 32, color: '#5470f2' }}>
                  <EnvironmentOutlined rev={false} style={{ fontSize: 20, fontWeight: '600' }} />
                </div>

              </div>
            </Link>
          </>
        }
        {props.role === 'HANHCHINH' &&
          <>
            <Link to={'booking-cancel-request'} className="management-item">
              <div>
                <p className='text-white fw-semibold'>Hủy đặt xe</p>
                <div className='bg-white rounded-circle d-flex align-items-center justify-content-center' style={{ width: 32, height: 32, color: '#5470f2' }}>
                  <WarningOutlined rev={false} style={{ fontSize: 20, fontWeight: '600' }} />
                </div>
              </div>
            </Link>
            <Link to={'/booking-request'} className="management-item">
              <div>
                <p className='text-white fw-semibold'>
                  Yêu cầu đặt xe
                </p>
                <div className='bg-white rounded-circle d-flex align-items-center justify-content-center' style={{ width: 32, height: 32, color: '#5470f2' }}>
                  <FileDoneOutlined rev={false} style={{ fontSize: 20, fontWeight: '600' }} />
                </div>

              </div>
            </Link>
          </>
        }
      </div>
    </div >

  )
}

export default Management