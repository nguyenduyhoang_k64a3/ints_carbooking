import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, NavLink, useHistory, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { DEFAULT_LANGUAGE } from 'locales/i18n';
import { Epath } from 'app/routes/routesConfig';
import { HomeOutlined, UserOutlined, CarOutlined, FileDoneOutlined } from '@ant-design/icons';
import { UserContext } from 'app/App';

function Header() {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();

  const onLogOut = () => {
    i18n.changeLanguage(DEFAULT_LANGUAGE);
    history.push(Epath.loginPage);
  };

  const location = useLocation();
  const user = useContext(UserContext);

  return (

    <nav className="navbar fixed-bottom bg-white w-100" style={{ boxShadow: '0 -5px 11px rgba(206,206,206,.35)', maxWidth: '100%' }}>

      <div className="container-fluid  w-100 position-relative ">
        <NavLink
          className="nav-link nav-link-text" to={'/'}
          style={{ color: '' === location.pathname.split('/')[1] ? '#5470f2' : '#000' }}
        >
          <HomeOutlined style={{ fontSize: 28 }} rev />
        </NavLink>

        <NavLink
          className="nav-link nav-link-text"
          to={'/me'}
          style={{ color: 'me' === location.pathname.split('/')[1] ? '#5470f2' : '#000' }}
        >
          <UserOutlined style={{ fontSize: 28 }} rev />
        </NavLink>
        {user.role === 'HANHCHINH' &&
          <Link
            className='nav-link rounded-circle position-absolute d-flex align-items-center justify-content-center'
            style={{
              backgroundColor: '#5470f2',
              width: 64,
              height: 64,
              color: '#fff',
              left: '50%',
              transform: 'translateX(-50%)',
              top: '-32px'
            }}
            role='button' to={'/booking-request'}>
            <FileDoneOutlined
              className=''
              style={{ fontSize: 28 }} rev
            />
          </Link>

        }
        {
          user.role === "PHONGBAN" &&
          <Link
            className='nav-link rounded-circle position-absolute d-flex align-items-center justify-content-center'
            style={{
              backgroundColor: '#5470f2',
              width: 64,
              height: 64,
              color: '#fff',
              left: '50%',
              transform: 'translateX(-50%)',
              top: '-32px'
            }}
            role='button' to={'/car-booking'}>
            <CarOutlined
              className=''
              style={{ fontSize: 28 }} rev
            />
          </Link>


        }

      </div>

    </nav>

  );
}

export default Header;
