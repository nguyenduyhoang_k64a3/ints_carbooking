import React, { useState } from 'react'
import { JsxElement } from 'typescript';

type TabsProps = {
    defaultActiveTab?: number;
    items: Array<TabProps>;
    style?: Object;
    tabStyle?: any;
}
const Tabs: React.FC<TabsProps> = ({ defaultActiveTab, items, style, tabStyle }) => {
    const [activeTab, setActiveTab] = useState(defaultActiveTab);
    const handleTabChange = (id: number) => {
        setActiveTab(id)
    }
    console.log(items);

    return (
        <div className="mt-3">
            <div className="d-flex align-items-center justify-content-between w-100 px-3">
                {items.map((item, index) =>
                    <div
                        className={`tab-title rounded-200 d-flex align-items-center justify-content-center ${activeTab === item.id ? "tab-active" : ""}`}
                        key={index}
                        onClick={() => handleTabChange(item.id)}
                    >
                        {item.title}
                    </div>
                )}
            </div>
            <div className="tab-content px-3">
                {
                    items.map((item, index) =>
                        <div key={index} className="mt-3">
                            {item.id === activeTab &&
                                <div className='my-2'>{item.content}</div>
                            }

                        </div>
                    )
                }
            </div>

        </div>
    )
}

export default Tabs

export type TabProps = {
    title: string;
    content: any | JsxElement;
    id: number;
    style?: Object;
}
export const Tab: React.FC<TabProps> = ({ }) => {
    return (
        <div className="tab-title">

        </div>
    )
}

// create tab switching in reactjs-typescript ?