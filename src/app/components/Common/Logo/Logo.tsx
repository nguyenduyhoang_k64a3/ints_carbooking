import React from 'react'
import logo from '../../../resource/img/logo.svg'

const Logo: React.FC = () => {
    return (
        <div className='d-flex justify-content-center align-items-center flex-column'>
            <img src={logo} alt=''></img>
            <h2 className="text-center font-weight-bold text-uppercase mt-1 mb-2 text-uppercase" style={{ color: '#4865EF', fontWeight: '600', }}> Car Managerment</h2>
        </div>
    )
}

export default Logo