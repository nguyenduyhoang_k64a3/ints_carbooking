import React, { useState } from "react";
import { DeleteOutlined } from '@ant-design/icons';
import ModalCustom from "../Modal/ModalCutom";
import ModalBottom from "../Modal/ModalBottom";
import { InputDetailItem } from "app/pages/BookCarDetailPage/BookCarDetailPage";
import { FormProvider, SubmitHandler, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { cancelBooking, getUnapprovedBooking } from "store/reducer/bookingReducer";
import { isEmptyObject } from "app/helpers/common";
import { notification } from "antd";
import { Link } from "react-router-dom";
export type BookingItemProps = {
    carName: string;
    startDate: string;
    endDate: string;
    carImage?: string;
    bookingId: number;
    wait?: boolean;
}

export type CancelBookingProps = {
    reasonP2: string;
    id_booking: number;
}

type NotificationType = 'success' | 'info' | 'warning' | 'error';
const BookingItem: React.FC<BookingItemProps> = (data) => {
    const dispatch = useDispatch();
    const [show, setShow] = useState(false);
    const methods = useForm<CancelBookingProps>();
    const [api, contextHolder] = notification.useNotification();

    const openNotificationWithIcon = (type: NotificationType, message: string, description?: string) => {
        api[type]({
            message: message,
            description: description ? description : "",
            placement: 'bottomRight',
            style: {
                width: '100%',
                bottom: 48,


            }
        });
    };

    const onSubmit: SubmitHandler<CancelBookingProps> = async (formData) => {
        console.log("Cancel with reason", formData);

        if (isEmptyObject(methods.formState.errors)) {
            const result: any = await dispatch(cancelBooking({ ...formData, id_booking: +data.bookingId }))
            if (result.payload.statusCode === 400) {
                methods.setError('reasonP2', result.payload.message)

            } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
                openNotificationWithIcon('error', 'Yêu cầu hủy không thành công');
                setShow(false);
                dispatch(getUnapprovedBooking());
            } else {
                openNotificationWithIcon('success', 'Hủy đặt xe thành công');
                setShow(false);
                dispatch(getUnapprovedBooking());
            }
        }
    }
    return (

        <div className="travel-history-item w-100">

            <div className="w-100 d-flex align-items-center justify-content-between flex-row py-1">
                <Link to={`/booking-history/${data.bookingId}`} className="d-flex flex-row text-none">
                    <div
                        className=" travel-car-image bg-cover me-3"
                        style={{
                            backgroundImage: `url(${data.carImage ? data.carImage : "https://www.ibexinsure.com/assets/img/no-image.png"})`
                        }}
                    >
                    </div>

                    <div className="d-flex flex-column align-items-center justify-content-start">
                        <p className="text-dark mb-0 w-100 fw-bold">
                            {data.carName}
                        </p>
                        <p className="text-dark mb-0 w-100">
                            {data.startDate}, {data.endDate}
                        </p>
                    </div>
                </Link>
                {data.wait && <ModalBottom
                    title="Hủy yêu cầu đặt xe"
                    triggerModalBtn={<DeleteOutlined rev style={{ fontSize: 24, color: '#EB5757' }} />}
                    closeButton={true}
                    type="danger"
                    show={show}
                    setShow={setShow}
                >
                    <h5 className="mb-3 text-center">
                        Hãy nhập lý do hủy yêu cầu đặt xe
                    </h5>
                    <FormProvider {...methods} >

                        <InputDetailItem
                            name="reasonP2"
                            label="Lý do hủy yêu cầu"
                            type="text"
                            placeHolder="Nhập lý do"
                            rules={{
                                required: 'Vui lòng nhập lý do hủy yêu cầu'
                            }}
                        />

                        <div className="mt-3 mb-3 w-100 px-3 d-flex justify-content-between align-items-center">
                            <div
                                className="bg-disabled px-3 rounded-200  text-center py-2 col-5 me-1"
                                onClick={() => setShow(false)}
                            >
                                <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Hủy bỏ</span>
                            </div>
                            <div
                                className="bg-danger px-3 rounded-200  text-center py-2 col-5 ms-1"
                                onClick={methods.handleSubmit(onSubmit)}

                            >
                                <span
                                    className="mb-0 text-white font-sm"
                                    style={{
                                        lineHeight: '22px',
                                    }}
                                >
                                    Đồng ý
                                </span>
                            </div>
                        </div>

                    </FormProvider >
                </ModalBottom>}
            </div>
            {contextHolder}
        </div>
    )
}

export default BookingItem;