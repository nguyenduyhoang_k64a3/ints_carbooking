import React from 'react';
import { Card } from 'antd';
import { TCar } from 'types/common';
import { Link } from 'react-router-dom';

const { Meta } = Card;


const CarItemCard: React.FC<TCar> = (car) => (
    <div className="w-100 px-3 ">
        <div className='w-100 flex-row d-flex car-item-card'>
            <div
                style={{
                    backgroundImage: `url("${car.carImage ? car.carImage : 'https://www.ibexinsure.com/assets/img/no-image.png'}")`,
                }}
                className="rounded-md flex col-5 car-item-image me-2"
            >
            </div>

            <div className="row col-7">
                <div className='w-100 text-truncate text-dark py-2'>
                    <p className='fw-semibold mb-1 text-dark'>{car.carName}</p>
                    <p className='mb-1 text-dark'>{car.carLicensePlate}</p>
                    <p className='mb-0 text-dark'>{car.driverName}</p>

                    <div className='w-100 my-0 d-flex justify-content-end w-100'>
                        <Link to={`/car/${car.driverId}`} className='mb-1 text-right text-none' style={{ fontSize: 12, color: '#5470f2' }}>Chi tiết</Link>
                    </div>
                    <Link className=" text-none book-car-btn d-flex align-items-center justify-content-center w-100" to={`car-booking/${car.driverId}`} >
                        <p className="fw-semibold text-center text-white mb-0">
                            Đặt xe
                        </p>
                    </Link>

                </div>
            </div>

        </div>
    </div>
);

export default CarItemCard;
