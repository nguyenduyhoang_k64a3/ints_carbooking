import React from 'react'
import { BellFilled } from '@ant-design/icons'
import Icon from '@ant-design/icons/lib/components/Icon'
import { Space } from 'antd'
const Notifications = () => {
    return (
        <>
            <div
                className='text-white d-flex justify-content-center align-items-center postion-relative'
                style={{ fontSize: '24px' }}
                role='button'
            >
                <Space className='d-flex align-items-center' >
                    <BellFilled rev style={{ fontSize: 24 }} />
                </Space>
                <div
                    className="have-notifications bg-danger rounded-circle position-absolute"
                    style={{
                        width: 8,
                        height: 8,
                        right: 2

                    }}
                >

                </div>

            </div>
        </>
    )
}

export default Notifications