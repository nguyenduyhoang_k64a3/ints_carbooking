import React, { useState } from 'react';
import { Modal } from 'antd';
import { IndexedObject } from 'types/common';

type ModalProps = {
    title: string;
    triggerModalBtn: any;
    style?: IndexedObject;

}
const ModalCustom: React.FC<ModalProps> = (props) => {
    const [modalOpen, setModalOpen] = useState(false);
    const handleOpenModal = () => {
        console.log(modalOpen);
        console.log('open modal ...');

        setModalOpen(true);
    }

    return (
        <>
            <div
                onClick={handleOpenModal}
            >
                {props.triggerModalBtn}
            </div>
            <Modal
                title={props.title}
                style={props.style}
                open={modalOpen}
                onOk={() => setModalOpen(false)}
                onCancel={() => setModalOpen(false)}
            >
                {props.children}
            </Modal>

        </>
    );
};

export default ModalCustom;