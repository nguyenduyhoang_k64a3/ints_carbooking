import { type } from 'os';
import React, { useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
// import { Modal } from 'antd';
import { IndexedObject } from 'types/common';

type ModalProps = {
    title: string;
    triggerModalBtn: any;
    style?: IndexedObject;
    show: boolean;
    setShow: any;
    type?: string;
    // confirmButton: string;
    closeButton?: boolean;

}
const ModalBottom: React.FC<ModalProps> = (props) => {
    // const [modalOpen, setModalOpen] = useState(false);
    const handleOpenModal = () => {
        console.log(props.show);
        console.log('open modal ...');

        props.setShow(true);
    }

    const handleCloseModal = () => {
        props.setShow(false);
    }

    // const handleModalConfirm = (data: { reasonP2: string }) => {
    //     props.handleConfirm()
    // }

    return (
        <>
            <div className=''
                onClick={handleOpenModal}
            >
                {props.triggerModalBtn}
            </div>
            <Modal show={props.show} onHide={handleCloseModal} bsPrefix='modal' style={{ top: 'auto', bottom: 0, left: 0, right: 0 }}>
                <Modal.Header closeButton={props.closeButton} className='text-center'>
                    <Modal.Title className={`${props.type === 'danger' ? 'text-danger' : 'text-main'}`}>{props.title}</Modal.Title>
                </Modal.Header>
                <div className='w-100 px-3'>
                    {props.children}
                </div>

            </Modal>

        </>
    );
};

export default ModalBottom;