import React from 'react'
import { useHistory } from 'react-router-dom';

const PageHeader: React.FC<{ text: string }> = ({ text }) => {
    console.log(text);

    const history = useHistory()
    const handleBack = () => {
        history.go(-1);
    }
    return (
        <div className='page-header bg-white w-100'>
            <div className="position-relative w-100  d-flex align-items-center justify-content-start flex-row">
                < div
                    className="page-header__back-btn text-main d-flex align-items-center justify-content-center "
                    onClick={handleBack}
                >
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-chevron-left" viewBox="0 0 16 16" strokeWidth={4}>
                        <path fillRule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                    </svg>
                </ div>
                <div
                    className='container w-100'
                    style={{ marginLeft: '-36px' }}
                >
                    <h3 className='w-100 mb-0 text-center text-main font-20 fw-bold'>{text}</h3>
                </div>
            </div>
        </div >
    )
}

export default PageHeader