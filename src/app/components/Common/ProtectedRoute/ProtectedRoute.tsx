import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import { Epath } from 'app/routes/routesConfig';
import { Role } from 'app/routes/routes';

interface ProtectedRouteProps extends RouteProps {
    requiredRole?: Array<Role>;
    component: React.ComponentType<any>;
}

const ProtectedRoute: React.FC<ProtectedRouteProps> = ({ requiredRole, component: Component, ...rest }) => {
    const userData: any = getUserFromLocalStorage();

    return (
        <Route {...rest} render={(props) => (
            requiredRole ? (requiredRole.includes(userData.role)
                ? <Component {...props} />
                : <Redirect to={Epath.noPermissionPage} />)
                : <Component {...props} />
        )} />
    );
};

export default ProtectedRoute;
