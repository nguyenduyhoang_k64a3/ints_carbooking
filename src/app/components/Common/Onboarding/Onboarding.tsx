import { systemColor } from 'app/helpers/common';
import React from 'react'
import { Link } from 'react-router-dom';
import { string } from 'yup'
export type OnboardingProps = {
    id: number;
    image: string;
    heading: string;
    description: string;
}
const Onboarding: React.FC<{ ob: OnboardingProps; next: Function }> = (props) => {
    const { id, image, heading, description } = props.ob;
    console.log(props);

    return (
        <div className='h-100 w-100 d-flex align-items-center justify-content-center flex-column'>
            <div className='w-100 d-flex align-items-center justify-content-center'>
                <img src={image} alt="" className='mx-auto' />
            </div>
            <h1 style={{ marginTop: 76, fontSize: 34 ,height:57}}>{heading}</h1>
            <p className='text-center w-75' style={{ marginTop: 27, fontSize: 19 }}>{description}</p>

            {id === 3 ?
                <Link
                    to='/choose-location'
                    className='text-white px-3 py-3 text-center'
                    style={{
                        fontFamily: 'Ubuntu-Regular',
                        backgroundColor: systemColor.primary,
                        width: 216,
                        borderRadius: 12,
                        marginTop: 75,
                        cursor: 'pointer'
                    }}
                >
                    Get started
                </Link> :

                <div
                    className="button px-3 py-3 d-flex justify-content-center"
                    style={{
                        fontFamily: 'Ubuntu-Regular',
                        backgroundColor: systemColor.primary,
                        width: 216,
                        borderRadius: 12,
                        marginTop: 75,
                        cursor: 'pointer'
                    }}
                    onClick={() => {
                        props.next(
                            (prev: number) => prev + 1
                        )
                    }}
                >
                    <span className='text-white'>Next</span>

                </div>
            }
        </div>
    )
}

export default Onboarding