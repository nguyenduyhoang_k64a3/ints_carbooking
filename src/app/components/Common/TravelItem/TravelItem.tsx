import { SendOutlined } from '@ant-design/icons';
import { systemColor } from 'app/helpers/common';
import React from 'react'
import { Link } from 'react-router-dom';

export type TravelItemProps = {
    carImage: string;
    timeStart: string;
    timeEnd: string;
    travelId: number;

}
const TravelItem: React.FC<TravelItemProps> = (props) => {
    return (
        <div className='travel-history-item w-100'>

            <div className="w-100 d-flex align-items-center justify-content-between flex-row py-1">
                <div
                    className=' travel-car-image bg-cover'
                    style={{ backgroundImage: `url(${props.carImage})` }}
                >

                </div>

                <div className='text-center d-flex align-items-center justify-content-center'>
                    <p className="text-dark mb-0">
                        {props.timeStart}, {props.timeEnd}
                    </p>
                </div>

                <Link to={`travel-history/${props.travelId}`}>
                    <SendOutlined rev style={{ fontSize: 24, color: systemColor.primary }} />
                </Link>
            </div>
        </div>
    )
}

export default TravelItem