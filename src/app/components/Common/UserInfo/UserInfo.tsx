import React from 'react'
import { TUser } from 'types/common'
import { Avatar, Space } from 'antd';
import { systemColor } from 'app/helpers/common';
import Notifications from '../Notifications/Notifications';

const UserInfo: React.FC<{ user: TUser }> = ({ user }) => {
    console.log(user.avatar);

    return (
        <div className='d-flex flex-row justify-content-between px-4'>
            <div className='d-flex flex-row'>
                <div className="avatar d-flex justify-content-center align-items-center me-2">

                    {user.avatar ?
                        <div

                            style={{
                                backgroundImage: `url(${user.avatar})`,
                                width: 36,
                                height: 36,
                                display: 'block',
                                borderRadius: 9999,
                                backgroundPosition: 'center',
                                backgroundSize: 'contain',
                                // backgroundRepeat: 'no-repeat',
                            }}
                        /> :
                        <div
                            className='d-flex justify-content-center align-items-center'
                            style={{ background: systemColor.grey, width: 36, height: 36, borderRadius: 9999 }}
                        >
                            <span className='text-white fw-bold'>
                                {user.fullname ? user.fullname.charAt(0) : 'K'}
                            </span>
                        </div>}

                </div>
                <div className="role d-flex flex-column justify-content-start align-items-start text-white">
                    <span>
                        Tài khoản {user.role}
                    </span>
                    <span className='fw-bold'>
                        {user.fullname}
                    </span>
                </div>
            </div>
            <div className="d-flex">
                <Notifications />
            </div>
        </div>
    )
}

export default UserInfo