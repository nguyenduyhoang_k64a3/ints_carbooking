import React from 'react';
import { Button, notification, Space } from 'antd';

type NotificationType = 'success' | 'info' | 'warning' | 'error';
type Props = {
    type: NotificationType;
    message: string;
    description?: string;
}
const Toast: React.FC<Props> = (props) => {
    const [api, contextHolder] = notification.useNotification();

    const openNotificationWithIcon = (type: NotificationType) => {
        api[type]({
            message: props.message,
            description: props.description
        });
    };

    return (
        <>
            {contextHolder}
            <Space>
                <Button onClick={() => openNotificationWithIcon('success')}>Success</Button>
                <Button onClick={() => openNotificationWithIcon('info')}>Info</Button>
                <Button onClick={() => openNotificationWithIcon('warning')}>Warning</Button>
                <Button onClick={() => openNotificationWithIcon('error')}>Error</Button>
            </Space>
        </>
    );
};

export default Toast;