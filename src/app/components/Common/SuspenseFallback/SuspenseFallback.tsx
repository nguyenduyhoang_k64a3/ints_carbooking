import React from 'react';
import logo from '../../../resource/img/logo.svg'
import Logo from '../Logo/Logo';
const SuspenseFallback = () => {
  return (<>
    <div style={{ height: '100vh' }} className='w-100'>
      <div className='h-100 d-flex justify-content-center align-items-center w-100'>

        <Logo />
      </div>

    </div>
  </>);
};

export default SuspenseFallback;
