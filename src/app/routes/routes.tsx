import React, { Suspense, Fragment, lazy } from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import { IndexedObject } from 'types/common';
import SuspenseFallback from '../components/Common/SuspenseFallback/SuspenseFallback';
import Layout, { PropsLayout } from '../components/Layout/Layout';
import { Epath } from './routesConfig';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import ProtectedRoute from 'app/components/Common/ProtectedRoute/ProtectedRoute';
export type Role = 'HANHCHINH' | 'PHONGBAN' | 'GIAMDOC' | 'TAIXE';
export type RoutesProps = {
  exact?: boolean;
  path: string;
  component: React.FC<{ history: IndexedObject; location: IndexedObject; match: IndexedObject }>;
  auth?: boolean;
  routes?: Array<RoutesProps>;
  layout?: React.FC<PropsLayout>;
  requiredRole?: Array<Role>;
};
const RenderRoutes = ({
  routes,
  isAuthenticated,
}: {
  routes: Array<RoutesProps>;
  isAuthenticated: boolean;

}) => {
  const user: any = getUserFromLocalStorage();
  return (
    <Suspense fallback={<SuspenseFallback />}>
      <Switch>
        {routes.map((route, i) => {
          const LayoutRoute = route.layout || Fragment;
          const Component = route.component || <div />;
          if (route.auth && !isAuthenticated) {
            return <Redirect key={i} to={Epath.loginPage} />;
          }
          return (
            <ProtectedRoute
              key={i}
              path={route.path}
              exact={!!route.exact}
              requiredRole={route.requiredRole}
              component={(props) => {
                return (
                  <LayoutRoute>
                    {route.routes ? (
                      <RenderRoutes routes={route.routes} isAuthenticated={isAuthenticated} />
                    ) : (
                      <Component {...props} />
                    )}
                  </LayoutRoute>
                );
              }}
            />
          );
        })}
      </Switch>
    </Suspense>
  );
};

export const routes: Array<RoutesProps> = [
  {
    exact: true,
    path: Epath.noPermissionPage,
    component: lazy(() => import('../pages/NoPermissionPage/NoPermissionPage')),
  },
  {
    exact: true,
    path: Epath.notFoundPage,
    component: lazy(() => import('../pages/NotFoundPage/NotFoundPage')),
  },
  {
    exact: true,
    path: Epath.loginPage,
    component: lazy(() => import('../pages/AuthPage/LoginPage/LoginPage')),
  },
  {
    exact: true,
    path: Epath.registerPage,
    component: lazy(() => import('../pages/AuthPage/RegisterPage/RegisterPage')),
  },
  {
    exact: true,
    path: Epath.bookCarResultPage,
    component: lazy(() => import('../pages/BookingResultPage/BookingResultPage')),
  },
  {
    path: '*',
    layout: Layout,
    component: () => <Redirect to={Epath.homePage} />,
    requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC', 'TAIXE'],
    routes: [
      {
        exact: true,
        path: Epath.homePage,
        component: lazy(() => import('../pages/HomePage/HomePage')),
        auth: true,
        requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC', 'TAIXE'],
      },
      {
        exact: true,
        path: Epath.userMenu,
        component: lazy(() => import('../pages/UserMenu/UserMenu')),
        auth: true,
        requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC', 'TAIXE'],
      },
      {
        exact: true,
        path: Epath.changePasswordPage,
        component: lazy(() => import('../pages/ChangePasswordPage/ChangePasswordPage')),
        auth: true,
        requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC', 'TAIXE'],
      },
      {
        exact: true,
        path: Epath.userInfoPage,
        component: lazy(() => import('../pages/UserInfoPage/UserInfoPage')),
        auth: true,
        requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC', 'TAIXE'],
      },
      {
        exact: true,
        path: Epath.userInfoEditPage,
        component: lazy(() => import('../pages/UserInfoPage/UserInfoEditPage')),
        auth: true,
        requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC', 'TAIXE'],
      },
      {
        exact: true,
        path: Epath.carDetailPage,
        component: lazy(() => import('../pages/CarDetailPage/CarDetailPage')),
        auth: true,
        requiredRole: ['PHONGBAN', 'TAIXE'],
      },
      {
        exact: true,
        path: Epath.travelHistoryPage,
        component: lazy(() => import('../pages/TravelHistoryPage/TravelHistoryPage')),
        auth: true,
        requiredRole: ['PHONGBAN'],
      },
      {
        exact: true,
        path: Epath.travelDetailPage,
        component: lazy(() => import('../pages/TravelDetailPage/TravelDetailPage')),
        auth: true,
        requiredRole: ['PHONGBAN', 'TAIXE'],
      },
      {
        exact: true,
        path: Epath.bookingHistoryPage,
        component: lazy(() => import('../pages/BookingHistoryPage/BookingHistoryPage')),
        auth: true,
        requiredRole: ['PHONGBAN'],
      },
      {
        exact: true,
        path: Epath.bookingRequestPage,
        component: lazy(() => import('../pages/BookingRequestPage/BookingRequestPage')),
        auth: true,
        requiredRole: ['HANHCHINH', 'GIAMDOC'],
      },
      {
        exact: true,
        path: Epath.bookingCancelRequestPage,
        component: lazy(() => import('../pages/CancelBookingRequestPage/CancelBookingRequestPage')),
        auth: true,
        requiredRole: ['HANHCHINH', 'GIAMDOC'],
      },
      {
        exact: true,
        path: Epath.bookingInfomationPage,
        component: lazy(() => import('../pages/BookingInfomationPage/BookingInfomationPage')),
        auth: true,
        requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC'],
      },
      {
        exact: true,
        path: Epath.bookCarPage,
        component: lazy(() => import('../pages/BookCarPage/BookCarPage')),
        auth: true,
        requiredRole: ['PHONGBAN'],
      },
      {
        exact: true,
        path: Epath.bookCarDetailPage,
        component: lazy(() => import('../pages/BookCarDetailPage/BookCarDetailPage')),
        auth: true,
        requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC', 'TAIXE'],
      },
      {
        exact: true,
        path: Epath.bookCarPage,
        component: lazy(() => import('../pages/BookCarPage/BookCarPage')),
        auth: true,
        requiredRole: ['PHONGBAN'],
      },
      {
        exact: true,
        path: '*',
        requiredRole: ['HANHCHINH', 'PHONGBAN', 'GIAMDOC', 'TAIXE'],
        component: () => <Redirect to={Epath.notFoundPage} />,
      },
    ],
  },
];

export default RenderRoutes;
