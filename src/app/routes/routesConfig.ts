export const Epath = {
  // NotFoundPage
  notFoundPage: '/notfound',
  // NotFoundPage
  noPermissionPage: '/noPermission',
  // LoginPage
  chooseLocationPage: '/choose-location',
  // LoginPage
  loginPage: '/login',

  // RegisterPage
  registerPage: '/register',

  // Register Confirm Page
  registerConfirmPage: '/register-confirm',

  // User menu page 
  userMenu: '/me',

  // User menu page 
  changePasswordPage: '/me/change-password',

  // Home Page
  homePage: '/',

  // Travel History Page
  travelHistoryPage: '/travel-history',

  // Travel Detail Page
  travelDetailPage: '/travel-history/:id',

  // Booking History Page
  bookingHistoryPage: '/booking-history',

  // Car Booking Page
  bookCarPage: '/car-booking',

  // Car Booking result Page
  bookCarResultPage: '/booking-result/:status',

  // Car Booking Detail Page
  bookCarDetailPage: '/car-booking/:id',

  // Car Detail Page
  carDetailPage: '/car/:id',

  // User info page
  userInfoPage: '/me/info',

  // User info edit page
  userInfoEditPage: '/me/info/edit',

  // User info edit page
  bookingRequestPage: '/booking-request',

  // User info edit page
  bookingCancelRequestPage: '/booking-cancel-request',

  // Booking detailed infomationn page
  bookingInfomationPage: '/booking-history/:booking_id',
};
