import moment from 'moment';
import React, { useEffect, createContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { getLanguage, getUserFromLocalStorage, saveLanguage } from './helpers/localStorage';
import RenderRoutes, { routes } from './routes/routes';
import './styles/variables.scss';
import './styles/animation.scss';
import './styles/base.scss';
import './styles/app.scss';
import './styles/elements.scss';
import './styles/typography.scss';
import './styles/dependencies/index.scss';
import "antd/dist/antd.css";
import 'leaflet/dist/leaflet.css';
import { RootState } from 'types/RootState';
// import { createContext } from 'vm';
import { TUser } from 'types/common';

// Set locate to moment lib
const language = getLanguage();
saveLanguage(language);
moment.locale(language);


export const UserContext = createContext<TUser | any>(getUserFromLocalStorage());
export function App() {
  const user: TUser | any = getUserFromLocalStorage();
  const dispatch = useDispatch();
  const { authenticated, currentUser } = useSelector(
    (state: RootState) => state.auth
  );

  const isAuthenticated = !!(authenticated && currentUser && Object.keys(currentUser).length);


  useEffect(() => {

    console.log('auth status: ', authenticated);
  }, [isAuthenticated, user]);
  // console.log(authenticated);

  return (
    <UserContext.Provider value={user}>
      <Router basename="">
        <RenderRoutes
          routes={routes}
          // isAuthenticated={!!(isAuthenticated || )}
          isAuthenticated={!!Object.keys(user).length}
        />
      </Router>
    </UserContext.Provider>
  );
}
function loadCommonData(): any {
  throw new Error('Function not implemented.');
}

