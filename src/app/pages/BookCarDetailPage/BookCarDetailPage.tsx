import PageHeader from '../../components/Common/PageHeader/PageHeader';
import React, { useEffect, useRef, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import { CalendarFilled, ClockCircleFilled } from '@ant-design/icons';
import { IndexedObject } from 'types/common';
import { FormProvider, SubmitHandler, useForm, useFormContext } from 'react-hook-form';
import { isEmptyObject } from 'app/helpers/common';
import { bookingRequest } from 'store/reducer/bookingReducer';
import { getCarById } from 'store/reducer/carReducer';


export type BookingDetail = {
    driver_id: number;
    date_start: string;
    time_start: string;
    date_end: string;
    time_end: string;
    location_start: string;
    location_end: string;
    numbers_passenger: number;
    position_title: string;
    fullname_booking: string;
    reason_booking: string;
    area?: "Nội thành" | "Ngoại Thành";
}
const BookCarDetailPage = () => {
    const slug: any = useParams();
    const dispatch = useDispatch();
    const history = useHistory();
    const carDetail = useSelector((state: RootState) => state.car.carDetail)
    console.log(slug);
    const [id, setId] = useState(slug.id);
    useEffect(() => {
        dispatch(getCarById(id))
    }, [])

    const methods = useForm<BookingDetail>();
    const onSubmit: SubmitHandler<BookingDetail> = async (data) => {
        if (isEmptyObject(methods.formState.errors)) {
            const result: any = await dispatch(bookingRequest({
                ...data,
                date_start: new Date(data.date_start).toISOString(),
                date_end: new Date(data.date_end).toISOString(),
                numbers_passenger: +data.numbers_passenger,
                driver_id: id
            }));
            console.log(result);

            if (result.payload.statusCode === 400) {
                methods.setError('date_start', { type: 'manual', message: result.payload.message });
            } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
                history.push('/booking-result/fail');
            } else {
                history.push('/booking-result/success');
            }
        }
    }

    return (
        <div className='w-100 page' >
            <PageHeader text='Thông tin đặt xe' />
            <div className='mt-5 w-100 px-3 '>
                <div className='bg-main px-3 rounded-200 mt-3 text-white text-center status-button-disabled'>
                    {carDetail.carName}
                </div>

            </div>
            <FormProvider {...methods}>
                <div className='mt-5 w-100 px-3 '>
                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập họ tên người đề nghị"
                        }}
                        label='Họ và tên người đề nghị'
                        placeHolder='Ghi đầy đủ họ tên'
                        name="fullname_booking"

                    />
                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập phòng ban chuyên môn"
                        }}
                        label='Phòng ban chuyên môn'
                        placeHolder='Nhập tên phòng ban'
                        name="department"

                    />
                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập lí do cần đặt xe"
                        }}
                        label='Lý do đặt xe'
                        placeHolder='Nhập lý do cần đặt xe'
                        name="reason_booking"

                    />

                    <InputDetailItem
                        rules={{
                            required: "Vui lòng chọn khu vực"
                        }}
                        label='Khu vực'
                        placeHolder='Chọn khu vực'
                        type='select'
                        name="area"
                        selectData={[
                            {
                                key: 'Nội Thành',
                                value: 'Nội Thành'
                            },
                            {
                                key: 'Ngoại Thành',
                                value: 'Ngoại Thành'
                            },
                        ]}
                    />

                    <InputDetailItem
                        label='Ngày đi dự kiến'
                        placeHolder='Nhập ngày đi dự kiến'
                        type='date'
                        icon={<CalendarFilled rev style={{ width: 24, height: 24, fontSize: 24 }} />}
                        rules={{
                            required: "Vui lòng nhập ngày đi dự kiến"

                        }}
                        name='date_start'
                    />

                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập thời gian đi dự kiến"
                        }}
                        label='Thời gian đi dự kiến'
                        placeHolder='Nhập Thời gian đi dự kiến'
                        type='time'
                        icon={<ClockCircleFilled rev style={{ width: 24, height: 24, fontSize: 24 }} />}

                        name="time_start"

                    />
                    <InputDetailItem
                        label='Ngày về dự kiến'
                        placeHolder='Nhập ngày về dự kiến'
                        type='date'
                        icon={<CalendarFilled rev style={{ width: 24, height: 24, fontSize: 24 }} />}
                        rules={{
                            required: "Vui lòng nhập ngày về dự kiến"
                        }}
                        name='date_end'
                    />
                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập thời gian về dự kiến"
                        }}
                        label='Thời gian về dự kiến'
                        placeHolder='Nhập thời gian về dự kiến'
                        icon={<ClockCircleFilled rev style={{ width: 24, height: 24, fontSize: 24 }} />}
                        name="time_end"
                        type='time'
                    />

                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập địa điểm xuất phát"
                        }}
                        label='Địa điểm xuất phát'
                        placeHolder='Chọn dịa điểm xuất phát'

                        name="location_start"

                    />

                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập địa điểm đến"
                        }}
                        label='Địa điểm đến'
                        placeHolder='Chọn địa điểm bạn muốn tới'

                        name="location_end"

                    />

                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập số người đi"
                        }}
                        label="Số người đi"
                        placeHolder="Nhập Số người đi"
                        type="number"
                        name="numbers_passenger"

                    />
                    <InputDetailItem
                        rules={{
                            required: "Vui lòng nhập chức danh người đi"
                        }}
                        label="Chức danh người đi"
                        placeHolder="Nhập Chức danh người đi"
                        name="position_title"

                    />

                </div>
                <div className="mt-3 w-100 px-3 d-flex justify-content-between align-items-center">
                    <div className="bg-danger px-3 rounded-200  text-center py-2 col-5 me-1">
                        <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Hủy bỏ</span>
                    </div>
                    <div
                        className="bg-main px-3 rounded-200  text-center py-2 col-5 ms-1"
                        onClick={methods.handleSubmit(onSubmit)}
                    >
                        <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Đặt xe</span>
                    </div>
                </div>
            </FormProvider>
        </div>
    )
}

export default BookCarDetailPage

export const InputDetailItem: React.FC<{
    label: string;
    placeHolder: string;
    inputValue?: any;
    icon?: any;
    type?: string;
    selectData?: Array<any>;
    bgColor?: string;
    disabled?: boolean;
    name: string;
    register?: any;
    rules?: IndexedObject;

}> = ({ label, placeHolder, inputValue, icon, type, selectData, bgColor, disabled, name, rules }) => {
    const id = makeid(5);
    const { register, formState: { errors }, setValue } = useFormContext();
    // const [showPassword, setShowPassword] = useState(false);
    // console.log(errors);
    if (inputValue) {
        setValue(name, inputValue);
    }

    return (
        <div className='w-100 mb-3'>
            <div
                className={`w-100 card-detail-item mb-1 rounded-200 ${errors[name] ? 'border-danger' : 'bordered-input-group'} `}
                style={{ backgroundColor: bgColor }}
            >
                <div className="w-100 d-flex align-items-start justify-content-center flex-column position-relative">
                    <p className="mb-0 font-sm" style={{ color: '#909090' }}>{label}</p>
                    {
                        type === 'select'
                            ?
                            <>
                                <label htmlFor={id} className='w-100 book-car-detail-label'>

                                    <select

                                        id={id}
                                        className='book-car-select w-100 '
                                        disabled={disabled}
                                        {...register(name, { ...rules })}
                                    >
                                        {selectData?.map((data, index) => {
                                            return (
                                                <option className='text-grey' key={index} value={data.key}>{data.value}</option>
                                            )
                                        })}
                                    </select>
                                </label>
                            </>
                            :
                            <>

                                <input
                                    id={id}

                                    className={`w-100 border-none outline-none mb-0 ${disabled ? 'text-disabled' : 'text-dark'}`}
                                    type={type}
                                    placeholder={placeHolder}
                                    style={{ backgroundColor: '#f4f5f7', height: 30 }}
                                    {...register(name, { ...rules })}
                                    disabled={disabled}
                                >
                                </input>
                                <label htmlFor={id} className="booking-detail-icon">
                                    {icon}
                                </label>
                            </>
                    }
                </div>
            </div>
            {errors[name] && <span className='text-danger ms-3'>{errors[name]?.message}</span>}
        </div>
    )
}

function makeid(length: number) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        counter += 1;
    }
    return result;
}