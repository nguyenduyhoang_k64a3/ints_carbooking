import BookingItem from 'app/components/Common/BookingItem/BookingItem';
import PageHeader from 'app/components/Common/PageHeader/PageHeader';
import Tabs, { TabProps } from 'app/components/Common/Tabs/Tabs';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getApprovedBooking, getBookingRequestIncity, getBookingRequestOutcity } from 'store/reducer/bookingReducer';
import { RootState } from 'types/RootState';

const CancelBookingRequestPage = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const user: any = getUserFromLocalStorage();
    const requestListIncity = useSelector((state: RootState) => state.booking.listWaitInCity);
    const requestListOutcity = useSelector((state: RootState) => state.booking.listWaitOutCity);
    const requestListApproved = useSelector((state: RootState) => state.booking.listApproved);
    useEffect(() => {
        const fetchBookingRequestIncity = async () => {
            const result: any = await dispatch(getBookingRequestIncity());
            console.log(result);
            if (result.payload.statusCode === 401) {
                history.push("/login");
            } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
                console.log(result.payload.message);

            }
        };
        const fetchBookingRequestOutcity = async () => {
            const result: any = await dispatch(getBookingRequestOutcity());
            console.log(result);
            if (result.payload.statusCode === 401) {
                history.push("/login");
            } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
                console.log(result.payload.message);

            }
        };

        user.role === 'HANHCHINH' && fetchBookingRequestIncity();
        user.role === 'GIAMDOC' && fetchBookingRequestOutcity();

    }, []);

    useEffect(() => {
        const fetchBookingRequestApproved = async () => {
            const result: any = await dispatch(getApprovedBooking());
            console.log(result);
            if (result.payload.statusCode === 401) {
                history.push("/login");
            } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
                console.log(result.payload.message);

            }
        };
        fetchBookingRequestApproved()
    }, []);

    const tabs: Array<TabProps> = [
        {
            id: 1,
            title: 'Chờ duyệt',
            content: <>
                {
                    requestListIncity.length > 0
                        ?
                        [...requestListIncity, ...requestListOutcity].map((item, index) => {
                            return (
                                <BookingItem
                                    bookingId={item.id_booking}
                                    key={index}
                                    carImage={item.driver.vehicle.image_vehicle}
                                    carName={item.reason_booking}
                                    startDate={`${new Date(item.date_start).toLocaleDateString()} ${item.time_start}`}
                                    endDate={`${new Date(item.date_end).toLocaleDateString()} ${item.time_end}`}
                                />
                            )
                        })
                        : <p className='px-3'>Không tìm thấy yêu cầu chờ duyệt nào</p>
                }
            </>
        },
        {
            id: 2,
            title: 'Đã duyệt',
            content: <>
                {
                    requestListApproved.length > 0
                        ?
                        requestListApproved.map((item, index) => {
                            return (
                                <BookingItem
                                    bookingId={item.id_booking}
                                    key={index}
                                    carImage={item.driver.vehicle.image_vehicle}
                                    carName={item.reason_booking}
                                    startDate={`${new Date(item.date_start).toLocaleDateString()} ${item.time_start}`}
                                    endDate={`${new Date(item.date_end).toLocaleDateString()} ${item.time_end}`}
                                />
                            )
                        })
                        : <p className='px-3'>Không tìm thấy yêu cầu đã duyệt nào</p>
                }
            </>
        },
        {
            id: 3,
            title: 'Đã hủy',
            content: ''
        }

    ]

    return (
        <div className='w-100 page'>
            <PageHeader text='Yêu cầu hủy đặt xe' />
            <div className="w-100">
                <Tabs items={tabs} defaultActiveTab={1} />
            </div>
        </div>
    );
}

export default CancelBookingRequestPage