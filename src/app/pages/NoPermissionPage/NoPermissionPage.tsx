import React from 'react';

type Props = {};

const NoPermissionPage = (props: Props) => {
  return <div>Bạn không có quyền truy cập trang này</div>;
};

export default NoPermissionPage;
