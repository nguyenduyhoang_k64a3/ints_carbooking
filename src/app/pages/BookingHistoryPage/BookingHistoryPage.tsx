import { SearchOutlined } from '@ant-design/icons'
import BookingItem from 'app/components/Common/BookingItem/BookingItem'
import PageHeader from 'app/components/Common/PageHeader/PageHeader'
import Tabs, { TabProps } from 'app/components/Common/Tabs/Tabs'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { getCanceledBooking, getUnapprovedBooking, listWait } from 'store/reducer/bookingReducer'
import { RootState } from 'types/RootState'


const BookingHistoryPage = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const listUnapproved = useSelector(listWait);
  const listCanceled = useSelector((state: RootState) => state.booking.listCancelled)

  useEffect(() => {
    const fetchWaitList = async () => {
      const result: any = await dispatch(getUnapprovedBooking());
      console.log(result);
      if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
        history.push("/login");
      }
    }
    const fetchCancelledList = async () => {
      const result: any = await dispatch(getCanceledBooking());
      console.log(result);
      if (result.payload.statusCode === 401) {
        history.push("/login");
      }
    }
    fetchWaitList();
    fetchCancelledList();
  }, []);

  console.log("List wait: ", listUnapproved);


  const tabs: Array<TabProps> = [
    {
      id: 1,
      title: "Chờ duyệt",
      content: <>
        {
          listUnapproved.length > 0 ?
            listUnapproved.map((item, index) => {
              return (
                <BookingItem
                  bookingId={item.id_booking}
                  key={index}
                  carImage={item.driver.vehicle.image_vehicle}
                  carName={item.reason_booking}
                  startDate={`${new Date(item.date_start).toLocaleDateString()} ${item.time_start}`}
                  endDate={`${new Date(item.date_end).toLocaleDateString()} ${item.time_end}`}
                  wait
                />
              )
            })
            :
            <span className='px-3'>Không tìm thấy yêu cầu đặt xe nào.</span>
        }


      </>,

    },
    {
      id: 2,
      title: "Đã hủy",
      content: <>
        {
          listCanceled.length > 0 ?
            listCanceled.map((item, index) => {
              return (
                <BookingItem
                  bookingId={item.booking.id_booking}
                  key={index}
                  carImage={item.booking.driver.vehicle.image_vehicle}
                  carName={item.booking.reason_booking}
                  startDate={`${new Date(item.booking.date_start).toLocaleDateString()} ${item.booking.time_start}`}
                  endDate={`${new Date(item.booking.date_end).toLocaleDateString()} ${item.booking.time_end}`}
                  wait={false}
                />
              )
            })
            :
            <span className='px-3'>Không tìm thấy yêu cầu đặt xe nào.</span>
        }
      </>,

    },
    {
      id: 3,
      title: "Chờ hủy",
      content: <>
        {
          listUnapproved
            .filter((item, index) => {
              return item.status_booking === "Chờ hủy";
            })
            .map((item, index) => {
              return (
                <p className="text-dark" key={index}>
                  Booking number {item.id_booking} with reason {item.reason_booking}
                </p>
              )
            })
        }
      </>,

    },
    {
      id: 4,
      title: "Bị từ chối",
      content: <>
        {
          listUnapproved
            .filter((item, index) => {
              return item.status_booking === "Bị từ chối";
            })
            .map((item, index) => {
              return (
                <p className="text-dark" key={index}>
                  Booking number {item.id_booking} with reason {item.reason_booking}
                </p>
              )
            })
        }
      </>,

    },
  ];


  return (
    <div className='w-100 page'>
      <PageHeader text='Lịch sử đặt xe' />
      <div className="w-100 px-3 mt-3 position-relative">
        <input className="search-car-input border-main outline-none w-100" placeholder='Tìm kiếm'>

        </input>
        <div className='search-car-input-icon'>
          <SearchOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />
        </div>
      </div>
      <div className="w-100">
        <Tabs items={tabs} defaultActiveTab={1} />
      </div>
    </div>
  )
}

export default BookingHistoryPage