import React, { useContext } from 'react'

import UserInfo from 'app/components/Common/UserInfo/UserInfo'
import { UserContext } from 'app/App'
import Management from 'app/components/Management/Management';
import SuggestedCar from 'app/components/SuggestedCar/SuggestedCar';
const DriverHomePage = () => {

    const user = useContext(UserContext);
    return (
        <div className=''>
            <UserInfo user={user} />
            <div className="bg-white w-100 management mt-5 ">
                <Management role={user.role} title='Quản lý' />
                <SuggestedCar role={user.role} title='Chuyến đi sắp tới'/>
            </div>
        </div>
    )
}

export default DriverHomePage