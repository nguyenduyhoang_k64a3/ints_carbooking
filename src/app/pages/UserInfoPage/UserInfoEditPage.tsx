import PageHeader from 'app/components/Common/PageHeader/PageHeader';
import { getUserFromLocalStorage, saveUser } from 'app/helpers/localStorage';
import React, { useEffect, useState } from 'react';
import { InputDetailItem } from '../BookCarDetailPage/BookCarDetailPage';
import { CameraOutlined } from '@ant-design/icons';
import { updateAvatar, updateInfo } from 'store/reducer/userReducer';
import { useHistory } from 'react-router-dom';
import { notification } from 'antd';
import { useDispatch } from 'react-redux';
import { FormProvider, useForm } from 'react-hook-form';
import { isEmptyObject } from 'app/helpers/common';
import ModalBottom from 'app/components/Common/Modal/ModalBottom';

type NotificationType = 'success' | 'info' | 'warning' | 'error';
export type FormInfoData = {
    fullname: string;
    department: string;
};
export type UploadAvatar = {
    avatar: string | File | null;
}
const UserInfoPage: React.FC = () => {
    console.log('re-render...');

    const [show, setShow] = useState(false);
    const [api, contextHolder] = notification.useNotification();
    const openNotificationWithIcon = (type: NotificationType, message: string, description?: string) => {
        api[type]({
            message: message,
            description: description ? description : "",
            placement: 'bottomRight',
            style: {
                width: '100%',
                bottom: 48,
            }
        });
    };

    const user: any = getUserFromLocalStorage();
    const history = useHistory();
    const dispatch = useDispatch();
    // const [fullName, setFullName] = useState<string>(user.fullname);
    // const [department, setDepartment] = useState<string>(user.department.name_department);
    const [avatar, setAvatar] = useState<string | null>(user.avatar ? user.avatar : "");
    const [file, setFile] = useState<File | null>(null);
    const methods = useForm<FormInfoData>();
    const handleCancel = () => {
        setShow(false);
    };

    const formData = new FormData();
    const handleAvatarChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) {
            setFile(file); // set file
            console.log('uploaded photo: ', file);
            setAvatar(URL.createObjectURL(file))
            setShow(false);
        }
    }

    const onSubmit = async (data: FormInfoData) => {
        console.log('Form data:', data);
        if (isEmptyObject(methods.formState.errors)) {
            const info: any = await dispatch(updateInfo(data));

            if (info.payload.statusCode === 401) {
                alert('Phiên đăng nhập đã hết hạn');
                history.push('/login');
            } else if (info.payload.statusCode < 200 || info.payload.statusCode >= 300) {
                methods.setError('fullname', { type: 'manual', message: info.payload.message });

            } else {
                saveUser(info.payload);
            }
            if (file) {
                formData.append('avatar', file);
            }
            const avatar: any = await dispatch(updateAvatar(formData));
            // console.log(avatar);

            if (avatar.payload.statusCode === 401) {
                alert('Phiên đăng nhập đã hết hạn');
                history.push('/login');
            } else if (avatar.payload.statusCode < 200 || avatar.payload.statusCode >= 300) {
                // methods.setError('fullname', { type: 'manual', message: avatar.payload.message });
                openNotificationWithIcon('error', 'Cập nhật ảnh đại diện không thành công');
            } else {
                saveUser(avatar.payload);
            }
            console.log(info);

            // openNotificationWithIcon('success', 'Cập nhật thông tin thành công');    
        }
    }

    return (
        <>

            <div className="w-100 page">
                <input
                    type="file"
                    id="avatar"
                    className='d-none'
                    onChange={handleAvatarChange}
                />
                <PageHeader text='Thông tin cá nhân' />
                <div className="user-info w-100 d-flex align-items-center justify-content-center flex-column">
                    <div
                        className="rounded-circle user-avatar position-relative"
                        style={{
                            width: 80,
                            height: 80,
                            backgroundImage: `url(${avatar})`,
                        }}>
                        <ModalBottom
                            title="Đổi ảnh đại diện"
                            show={show}
                            setShow={setShow}
                            triggerModalBtn={
                                <div
                                    className="d-flex bg-white position-absolute update-avatar-button rounded-circle d-flex justify-content-center align-items-center"
                                >
                                    <CameraOutlined rev style={{ fontSize: 18, color: '#5470f2' }} />
                                </div>
                            }
                        >
                            <div className="w-100 px-3 d-flex align-items-center justify-content-center my-3">
                                <div
                                    style={{ lineHeight: 1.5 }}
                                    className='mx-4 px-3 py-2 text-none bg-main text-white rounded-button'
                                    onClick={handleCancel}
                                >
                                    Chụp ảnh
                                </div>
                                <div
                                    style={{ lineHeight: 1.5 }}
                                    className='mx-4 px-3 py-2 text-none bg-main text-white rounded-button'
                                >
                                    <label htmlFor="avatar">Tải ảnh lên</label>
                                </div>
                            </div>
                        </ModalBottom>
                    </div>
                </div>
                <div className='mt-3 w-100 px-3 d-flex justify-content-end align-items-center flex-column'>
                    <FormProvider {...methods} >
                        <form >
                            <InputDetailItem
                                label='Họ và tên'
                                placeHolder='Nhập họ và tên'
                                name='fullname'
                                inputValue={user.fullname}
                            />
                            <InputDetailItem
                                label='Thông tin phòng ban'
                                placeHolder='Nhập tên phòng ban'
                                inputValue={user.department.name_department}
                                disabled={true}
                                name='department'

                            />
                            <div className="w-100 d-flex align-items-center justify-content-between">
                                <div
                                    style={{ lineHeight: 1.5 }}
                                    className='mx-4 px-5 py-2 text-none bg-danger text-white rounded-button'
                                    onClick={() => {
                                        history.goBack()
                                    }}
                                >
                                    Hủy bỏ
                                </div>
                                {contextHolder}
                                <div
                                    role='button'
                                    style={{ lineHeight: 1.5 }}
                                    className='mx-4 px-5 py-2 text-none bg-main text-white rounded-button'
                                    onClick={methods.handleSubmit(onSubmit)}
                                >
                                    Lưu lại
                                </div>
                            </div>
                        </form>
                    </FormProvider>
                </div>
            </div>
        </>
    )
}

export default UserInfoPage;