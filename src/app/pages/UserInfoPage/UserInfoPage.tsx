import PageHeader from 'app/components/Common/PageHeader/PageHeader';
import { getUserFromLocalStorage } from 'app/helpers/localStorage'
import React from 'react'
// import { TUser } from 'types/common';
import { DetailItem } from '../CarDetailPage/CarDetailPage';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
const UserInfoPage: React.FC = () => {
    
    const user: any = getUserFromLocalStorage();
    console.log(user);

    return (
        <>

            <div className='w-100 page'>
                <PageHeader text='Thông tin cá nhân' />
                <div className="user-info w-100 d-flex align-items-center justify-content-center flex-column">
                    <div
                        className="rounded-circle user-avatar"
                        style={{
                            width: 60,
                            height: 60,
                            backgroundImage: `url(${user.avatar})`
                        }}>

                    </div>

                </div>
                <div className='mt-3 w-100 px-3 d-flex justify-content-end align-items-center flex-column'>
                    <DetailItem title='Họ và tên' content={user.fullname} />
                    <DetailItem title='Số điện thoại' content={user.username} />
                    <DetailItem title='Thông tin phòng ban' content={"Phòng " + user.department.name_department} />
                    <DetailItem title='Mã Nhân viên' content={user.id_employee} />
                </div>
                <div className="w-100 d-flex align-items-center justify-content-center">
                    <Link
                        to={'/me/info/edit'}
                        className='px-5 py-2 text-none bg-main text-white rounded-button'
                    >
                        Chỉnh sửa
                    </Link>
                </div>
            </div>
        </>
    )
}

export default UserInfoPage