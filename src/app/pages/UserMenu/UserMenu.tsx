import PageHeader from 'app/components/Common/PageHeader/PageHeader';
import {
  getUserFromLocalStorage,
  removeRefreshToken,
  removeToken,
  removeUser
} from 'app/helpers/localStorage';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { BookOutlined, ClockCircleOutlined, EnvironmentOutlined, FileDoneOutlined, LockOutlined, LogoutOutlined, ShareAltOutlined, UserOutlined, WarningOutlined } from '@ant-design/icons';
import ModalBottom from 'app/components/Common/Modal/ModalBottom';
import { Role } from 'app/routes/routes';
import { Epath } from 'app/routes/routesConfig';


const UserMenu = () => {
  const history = useHistory();
  const [showLogoutModal, setShowLogoutModal] = useState(false);
  const handleLogout = () => {
    removeToken();
    removeRefreshToken();
    removeUser();
    history.push('/login');
  };
  const user: any = getUserFromLocalStorage();
  const menus: Array<MenuItemProps> = [
    {
      text: 'Thông tin người dùng',
      icon: <UserOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        history.push(Epath.userInfoPage)
      },
      requiredRole: ['PHONGBAN', 'HANHCHINH', 'GIAMDOC', 'TAIXE']
    },
    {
      text: 'Lịch sử chuyến đi',
      icon: <EnvironmentOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        history.push(Epath.travelHistoryPage);
      },
      requiredRole: ['PHONGBAN'],
    },
    {
      text: 'Lịch sử đặt xe',
      icon: <ClockCircleOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        history.push(Epath.bookingHistoryPage);
      },
      requiredRole: ['PHONGBAN'],
    },
    {
      text: 'Hủy đặt xe',
      icon: <WarningOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        history.push(Epath.bookingCancelRequestPage)
      },
      requiredRole: ['HANHCHINH', 'GIAMDOC']
    },
    {
      text: 'Danh sách đặt xe',
      icon: <FileDoneOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        history.push(Epath.bookingRequestPage)
      },
      requiredRole: ['HANHCHINH', 'GIAMDOC']
    },
    {
      text: 'Đổi mật khẩu',
      icon: <LockOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        history.push(Epath.changePasswordPage)
      },
      requiredRole: ['PHONGBAN', 'HANHCHINH', 'GIAMDOC', 'TAIXE']
    },
    {
      text: 'Chia sẻ',
      icon: <ShareAltOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        console.log('Shared cho t ddi naof');
      },
      requiredRole: ['PHONGBAN', 'HANHCHINH', 'GIAMDOC', 'TAIXE']
    },
    {
      text: 'Giới thiệu',
      icon: <BookOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        console.log('Giới thiệu cho t ddi naof');
      },
      requiredRole: ['PHONGBAN', 'HANHCHINH', 'GIAMDOC', 'TAIXE']
    },
    {
      text: 'Đăng xuất',
      icon: <LogoutOutlined rev style={{ fontSize: 20, color: '#5470f2' }} />,
      handleClick: () => {
        setShowLogoutModal(true);
      },
      requiredRole: ['PHONGBAN', 'HANHCHINH', 'GIAMDOC', 'TAIXE']
    },

  ];
  return (
    <div className='w-100 page bg-main'>
      <PageHeader text='Cài đặt người dùng' />
      <div className="user-info w-100 d-flex align-items-center justify-content-center flex-column">
        <div
          className="rounded-circle user-avatar"
          style={{
            width: 60,
            height: 60,
            backgroundImage: `url(${user.avatar})`
          }}>

        </div>
        <div className='mt-2 user-fullname'>
          <p className='mb-0 text-white fw-semibold '>
            {user.fullname}
          </p>
        </div>
        <div className='user-phone'>
          <p className='text-white'>
            {user.username}
          </p>
        </div>
      </div>
      <div className='d-flex align-items-center w-100 px-3 flex-column bg-white content-rounded-top px-3 py-3'>
        {menus.map((menu, index) => {
          return (
            menu.requiredRole?.includes(user.role)
            && <MenuItem
              key={index}
              text={menu.text}
              icon={menu.icon}
              handleClick={menu.handleClick}
              requiredRole={menu.requiredRole} />
          )
        })
        }
      </div>
      <ModalBottom
        show={showLogoutModal}
        title='Đăng xuất'
        triggerModalBtn={''}
        setShow={setShowLogoutModal}
        type='danger'
      >
        <div className="mt-3 mb-3 w-100 px-3 d-flex justify-content-between align-items-center">
          <div
            className="bg-disabled px-3 rounded-200  text-center py-2 col-5 me-1"
            onClick={
              () => setShowLogoutModal(false)
            }
          >
            <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Hủy bỏ</span>
          </div>
          <div
            className="bg-danger px-3 rounded-200  text-center py-2 col-5 ms-1"
            onClick={handleLogout}

          >
            <span
              className="mb-0 text-white font-sm"
              style={{
                lineHeight: '22px',
              }}
            >
              Đăng xuất
            </span>
          </div>
        </div>
      </ModalBottom>
    </div>
  )
}

type MenuItemProps = {
  text: string;
  icon: any;
  handleClick?: () => void;
  requiredRole: Array<Role>;
}
const MenuItem: React.FC<MenuItemProps> = (props) => {
  return (
    <>
      <div className="menu-item text-none w-100" onClick={props.handleClick}>
        <div className="menu-item-icon">
          {props.icon}
        </div>
        <p className="mb-0 text-dark fw-semibold">{props.text}</p>
      </div>
    </>
  )
}
export default UserMenu