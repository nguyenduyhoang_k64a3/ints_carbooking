import PageHeader from 'app/components/Common/PageHeader/PageHeader';
import React, { useEffect, useState } from 'react';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { BookingDetail, InputDetailItem } from '../BookCarDetailPage/BookCarDetailPage';
import { CalendarFilled, ClockCircleFilled } from '@ant-design/icons';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { cancelBooking, getBookingInfomation } from 'store/reducer/bookingReducer';
import { RootState } from 'types/RootState';
import { DetailItem } from '../CarDetailPage/CarDetailPage';
import ModalBottom from 'app/components/Common/Modal/ModalBottom';
import { CancelBookingProps } from 'app/components/Common/BookingItem/BookingItem';
import { isEmptyObject } from 'app/helpers/common';
import { notification } from 'antd';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import ManagerActionButton from './ManagerActionButton';

const BookingInfomationPage = () => {
    const methods = useForm<CancelBookingProps>();
    const user: any = getUserFromLocalStorage();
    const [show, setShow] = useState(false);
    const history = useHistory();
    const slug: any = useParams();
    const dispatch = useDispatch();
    const [bookingId, setBookingId] = useState(slug.booking_id);
    const bookingInfomation = useSelector((state: RootState) => state.booking.bookingInfo);
    useEffect(() => {
        const fetchData = async () => {
            const info: any = await dispatch(getBookingInfomation(+bookingId));
            if (info.payload.statusCode === 401) {
                alert('Phiên đăng nhập hết hạn');
                history.push('/login');
            }
        }
        fetchData()
    }, []);

    // console.log(new Date(bookingInfomation.booking?.date_start).toISOString().split('T')[0]);
    console.log(bookingInfomation);
    const [api, contextHolder] = notification.useNotification();
    type NotificationType = 'success' | 'info' | 'warning' | 'error';
    const openNotificationWithIcon = (type: NotificationType, message: string, description?: string) => {
        api[type]({
            message: message,
            description: description ? description : "",
            placement: 'bottomRight',
            style: {
                width: '100%',
                bottom: 48,


            }
        });
    };
    const onSubmit: SubmitHandler<CancelBookingProps> = async (formData) => {
        console.log("Cancel with reason", formData);

        if (isEmptyObject(methods.formState.errors)) {
            const result: any = await dispatch(cancelBooking({ ...formData, id_booking: bookingId }))
            if (result.payload.statusCode === 400) {
                methods.setError('reasonP2', result.payload.message)

            } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
                openNotificationWithIcon('error', 'Yêu cầu hủy không thành công');
                setShow(false);
            } else {
                setShow(false);
                openNotificationWithIcon('success', 'Hủy đặt xe thành công');
                history.push('/booking-history');
            }
        }
    }
    return (
        <div className='w-100 page'>
            <PageHeader text='Thông tin đặt xe' />
            <div className='mt-5 w-100 px-3 '>
                <div className='bg-warning px-3 rounded-200 mt-3 text-white text-center status-button-disabled'>
                    {bookingInfomation.booking?.status_booking}
                </div>
                <div className='bg-disabled px-3 rounded-200 mt-3 text-white text-center status-button-disabled'>
                    {bookingInfomation.booking?.driver.vehicle.type}
                </div>

            </div>

            <div className='mt-5 w-100 px-3 '>
                <DetailItem
                    content={bookingInfomation.booking?.fullname_booking}
                    title='Họ và tên người đề nghị'
                />

                <DetailItem
                    content={bookingInfomation.booking?.reason_booking}
                    title='Lý do đặt xe'

                />

                <DetailItem
                    content={bookingInfomation.booking?.area}
                    title='Khu vực'
                />

                <DetailItem
                    content={bookingInfomation.booking?.date_start ? new Date(bookingInfomation.booking.date_start).toISOString().split('T')[0] : ''}
                    icon={<CalendarFilled rev style={{ width: 24, height: 24, fontSize: 24 }} />}
                    title='Ngày đi dự kiến'
                />

                <DetailItem
                    content={bookingInfomation.booking?.time_start}
                    icon={<ClockCircleFilled rev style={{ width: 24, height: 24, fontSize: 24 }} />}
                    title='Thời gian đi dự kiến'
                />
                <DetailItem
                    content={bookingInfomation.booking?.date_start ? new Date(bookingInfomation.booking?.date_end).toISOString().split('T')[0] : ''}
                    title='Ngày về dự kiến'
                    icon={<CalendarFilled rev style={{ width: 24, height: 24, fontSize: 24 }} />}
                />
                <DetailItem
                    content={bookingInfomation.booking?.time_end}
                    title='Thời gian về dự kiến'
                    icon={<ClockCircleFilled rev style={{ width: 24, height: 24, fontSize: 24 }} />}
                />

                <DetailItem
                    content={bookingInfomation.booking?.location_start}
                    title='Địa điểm xuất phát'

                />

                <DetailItem
                    content={bookingInfomation.booking?.location_end}
                    title='Địa điểm đến'
                />

                <DetailItem
                    content={bookingInfomation.booking?.numbers_passenger}
                    title="Số người đi"
                />
                <DetailItem
                    content={bookingInfomation.booking?.position_title}
                    title="Chức danh người đi"
                />

            </div>
            {(bookingInfomation.booking?.status_booking === "Chờ duyệt" && (user.role === 'HANHCHINH' || user.role === 'GIAMDOC'))
                && <ManagerActionButton bookingId={bookingId} />}
            {(bookingInfomation.booking?.status_booking === "Chờ duyệt" && user.role === 'PHONGBAN') && <ModalBottom
                title="Hủy yêu cầu đặt xe"
                triggerModalBtn={
                    <div className="mt-3 w-100 px-3 d-flex justify-content-end align-items-center">
                        <div
                            className="bg-danger px-3 rounded-200  text-center py-2 col-5 ms-1"
                        >
                            <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Hủy yêu cầu</span>
                        </div>
                    </div>
                }
                closeButton={true}
                type="danger"
                show={show}
                setShow={setShow}
            >
                <h5 className="mb-3 text-center">
                    Hãy nhập lý do hủy yêu cầu đặt xe
                </h5>
                <FormProvider {...methods} >

                    <InputDetailItem
                        name="reasonP2"
                        label="Lý do hủy yêu cầu"
                        type="text"
                        placeHolder="Nhập lý do"
                        rules={{
                            required: 'Vui lòng nhập lý do hủy yêu cầu'
                        }}
                    />
                    <div className="mt-3 mb-3 w-100 px-3 d-flex justify-content-between align-items-center">
                        <div
                            className="bg-disabled px-3 rounded-200  text-center py-2 col-5 me-1"
                            onClick={() => setShow(false)}
                        >
                            <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Hủy bỏ</span>
                        </div>
                        <div
                            className="bg-danger px-3 rounded-200  text-center py-2 col-5 ms-1"
                            onClick={methods.handleSubmit(onSubmit)}

                        >
                            <span
                                className="mb-0 text-white font-sm"
                                style={{
                                    lineHeight: '22px',
                                }}
                            >
                                Đồng ý
                            </span>
                        </div>
                    </div>

                </FormProvider >
            </ModalBottom>}
            {contextHolder}
        </div>
    )
}

export default BookingInfomationPage;