import ModalBottom from 'app/components/Common/Modal/ModalBottom';
import React, { useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { InputDetailItem } from '../BookCarDetailPage/BookCarDetailPage';
import { isEmptyObject } from 'app/helpers/common';
import { cancelBooking, approveBooking } from 'store/reducer/bookingReducer';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

type CancelBooking = {
    reasonP2: string;
}
const ManagerActionButton: React.FC<{ bookingId: number }> = (props) => {
    const [show, setShow] = useState(false);
    const methods = useForm<CancelBooking>();
    const dispatch = useDispatch();
    const history = useHistory();
    const handleApprove = async () => {
        const res: any = await dispatch(approveBooking(props.bookingId));
        console.log(res);
        history.push('/booking-request');

    }
    const onSubmit = async (formData: CancelBooking) => {
        console.log("Cancel with reason", formData);

        if (isEmptyObject(methods.formState.errors)) {
            const result: any = await dispatch(cancelBooking({ ...formData, id_booking: props.bookingId }))
            if (result.payload.statusCode === 400) {
                methods.setError('reasonP2', result.payload.message)

            } else if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
                // openNotificationWithIcon('error', 'Yêu cầu hủy không thành công');
                setShow(false);
            } else {
                setShow(false);
                // openNotificationWithIcon('success', 'Hủy đặt xe thành công');
                history.push('/booking-request');
            }
        }
    }
    return (
        <div className='mt-3 w-100 px-3 d-flex justify-content-between align-items-center'>
            <div
                className="bg-main px-3 rounded-200  text-center py-2 col-5 ms-1"
                onClick={handleApprove}
            >
                <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Duyệt</span>
            </div>
            <ModalBottom
                title="Hủy yêu cầu đặt xe"
                triggerModalBtn={
                    <div
                        className="bg-danger px-3 rounded-200  text-center py-2 col-8 ms-1 float-end"
                        style={{ width: 164 }}
                    >
                        <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Hủy yêu cầu</span>
                    </div>
                }
                closeButton={true}
                type="danger"
                show={show}
                setShow={setShow}
            >
                <h5 className="mb-3 text-center">
                    Hãy nhập lý do hủy yêu cầu đặt xe
                </h5>
                <FormProvider {...methods} >

                    <InputDetailItem
                        name="reasonP2"
                        label="Lý do hủy yêu cầu"
                        type="text"
                        placeHolder="Nhập lý do"
                        rules={{
                            required: 'Vui lòng nhập lý do hủy yêu cầu'
                        }}
                    />
                    <div className="mt-3 mb-3 w-100 px-3 d-flex justify-content-between align-items-center">
                        <div
                            className="bg-disabled px-3 rounded-200  text-center py-2 col-5 me-1"
                            onClick={() => setShow(false)}
                        >
                            <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Hủy bỏ</span>
                        </div>
                        <div
                            className="bg-danger px-3 rounded-200  text-center py-2 col-5 ms-1"
                            onClick={methods.handleSubmit(onSubmit)}

                        >
                            <span
                                className="mb-0 text-white font-sm"
                                style={{
                                    lineHeight: '22px',
                                }}
                            >
                                Đồng ý
                            </span>
                        </div>
                    </div>

                </FormProvider >
            </ModalBottom>

        </div>
    )
}

export default ManagerActionButton;