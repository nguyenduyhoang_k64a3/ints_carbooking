import PageHeader from "app/components/Common/PageHeader/PageHeader";
import React, { useEffect, useState } from 'react';
import { EnvironmentOutlined, SendOutlined } from '@ant-design/icons';
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getBookingInfomation } from "store/reducer/bookingReducer";
import { RootState } from "types/RootState";
const TravelDetailPage = () => {
    const slug: any = useParams()
    const [id, setId] = useState(slug.id);
    const dispatch = useDispatch();
    const travelInfomation = useSelector((state: RootState) => state.booking.bookingInfo);
    useEffect(() => {
        const fetchTravelInfo = async () => {
            const result = await dispatch(getBookingInfomation(id));
            console.log(result);

        }
        fetchTravelInfo();
    }, [])
    return (
        <div className="w-100 page">
            <PageHeader text="Chi tiết chuyến đi" />
            <div className="mt-4 w-100 d-flex align-items-center justify-content-center px-3">
                <div className="rounded-button bg-disabled text-center w-100 text-white font-sm px-5 py-2">
                    Chuyến đi {slug.id} sẽ bắt đầu sau 30 phút nữa
                </div>
            </div>
            <div className="mt-3 w-100 px-3">
                <div className="w-100 rounded-button shadowed-button">
                    <div className="location-start">
                        <div className="w-100 d-flex align-items-center justify-content-between flex-row py-2 px-3">
                            <div className="d-flex">
                                <div
                                    className=" travel-car-image bg-cover"
                                    style={{ backgroundImage: `url("https://www.ibexinsure.com/assets/img/no-image.png")` }}
                                >
                                </div>
                                <div className="ms-2 d-flex align-items-start justify-content-start flex-column">
                                    <p className="text-dark mb-0">
                                        Điểm đón
                                    </p>
                                    <p className="text-dark mb-0">
                                        {travelInfomation.booking?.location_start}
                                    </p>
                                </div>
                            </div>


                            <SendOutlined rev style={{ color: "#5470f2", fontSize: 24 }} />
                        </div>
                    </div>
                    <div className="w-100 px-3">
                        <div className="divider my-2 bg-disabled w-100" style={{ height: 1 }}>

                        </div>
                    </div>
                    <div className="location-start">
                        <div className="w-100 d-flex align-items-center justify-content-between flex-row py-1 px-3">
                            <div className="d-flex">
                                <div
                                    className=" travel-car-image bg-cover"
                                    style={{ backgroundImage: `url("https://www.ibexinsure.com/assets/img/no-image.png")` }}
                                >

                                </div>

                                <div className="ms-2 d-flex align-items-start justify-content-start flex-column">
                                    <p className="text-dark mb-0">
                                        Điểm đến
                                    </p>
                                    <p className="text-dark mb-0">
                                        {travelInfomation.booking?.location_end}
                                    </p>
                                </div>
                            </div>
                            <EnvironmentOutlined rev style={{ color: "#5470f2", fontSize: 24 }} />
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-100 mt-5 px-3 rounded-200">
                {/* <MapContainer center={[21.028511, 105.804817]} zoom={13} scrollWheelZoom={false} style={{ height: "100%" }}>
                    <TileLayer
                        attribution="&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors"
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={[21.028511, 105.804817]}>
                        <Popup>
                            A pretty CSS3 popup. <br /> Easily customizable.
                        </Popup>
                    </Marker>
                </MapContainer> */}
            </div>
            <div className="w-100 d-flex justify-content-center align-items-end px-3 mt-3">
                <div className="rounded-button shadowed-button bg-white text-main w-100 px-5 py-3 text-center font-sm fw-semibold">
                    Liên hệ với tài xế
                </div>
            </div>
            <div className="w-100 d-flex justify-content-center align-items-end px-3 mt-3">
                <div className="rounded-button text-white shadowed-button bg-danger text-main w-100 px-5 py-3 text-center font-sm fw-semibold">
                    Hủy Chuyến đi
                </div>
            </div>
        </div>
    )
}

export default TravelDetailPage