import { useState } from 'react';
import React from 'react';
import onboarding1 from '../../resource/img/onboarding1.svg';
import onboarding2 from '../../resource/img/onboarding2.svg';
import onboarding3 from '../../resource/img/onboarding3.svg';
import Onboarding, { OnboardingProps } from 'app/components/Common/Onboarding/Onboarding';
import { systemColor } from 'app/helpers/common';
import BackIcon from 'app/components/Icons/BackIcon/BackIcon';
import registerThumb from '../../resource/img/registerThumb.svg'
type Props = {};
const IntroPage = (props: Props) => {
  const [currentOnboarding, setCurrentOnboarding] = useState(0)
  const obList: Array<OnboardingProps> = [
    {
      id: 1,
      image: onboarding1,
      heading: 'Request a ride',
      description: 'Request a ride get picked up by a nearby community driver'
    },
    {
      id: 2,
      image: onboarding2,
      heading: 'Confirm Your Driver',
      description: 'Huge drivers network helps you find comforable, safe and cheap ride'
    },
    {
      id: 3,
      image: onboarding3,
      heading: 'Track your ride',
      description: 'Know your driver in advance and be able to view current location in real time on the map'
    },
  ];
  return (
    <div className='w-100'>
      <div className='h-100 d-flex justify-content-center align-items-center w-100 py-3 px-5 flex-column position-relative'>

        <div
          className="mt-3 mb-5 cursor-pointer position-absolute"
          style={{ top: 24, left: 32 }}
          onClick={() => {
            setCurrentOnboarding((prev) => prev - 1 >= 0 ? prev - 1 : 0)
          }}
        >
          <BackIcon size={36} className='text-black' fontWeight={4} />
        </div>
        <div className='d-flex justify-content-center align-items-center flex-column'>
          <Onboarding ob={obList[currentOnboarding]} next={setCurrentOnboarding} />
          <div
            className="progress"
            style={{
              height: 8,
              width: 100,
              backgroundColor: systemColor.lightGrey,
              marginTop: 60,

            }}
          >
            <div
              className='progress'
              style={{
                height: 8,
                width: '33.333333%',
                backgroundColor: systemColor.primary,
                transform: `translateX(${Number(currentOnboarding * 100)}%)`

              }}
            >

            </div>

          </div>
        </div>
      </div>

    </div>
  );
};

export default IntroPage;
