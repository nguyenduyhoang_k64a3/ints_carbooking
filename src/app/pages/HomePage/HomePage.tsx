import { useContext, useState } from 'react';
import React from 'react';
import UserHomePage from '../UserHomePage/UserHomePage';
import { UserContext } from 'app/App';
import DriverHomePage from '../DriverHomePage/DriverHomePage';
import ManagerHomePage from '../ManageHomePage/ManagerHomePage';
type Props = {
  // role?: string;
};
const switchPageByRole = (role: string) => {
  switch (role) {
    case "PHONGBAN":
      return <UserHomePage />;
    case "HCNS":
      return <ManagerHomePage />;
    case "TAIXE":
      return <DriverHomePage />;
    default:
      return <UserHomePage />
  }
}
const HomePage = (props: Props) => {
  const user = useContext(UserContext)
  return (
    <div className="w-100 h-100 pt-3" style={{ backgroundColor: "#5470f2" }}>
      <div className="py-3" >
        {
          switchPageByRole(user.role)
        }
      </div>

    </div>
  );
};

export default HomePage;
