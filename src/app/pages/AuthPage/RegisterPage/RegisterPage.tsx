import BackIcon from 'app/components/Icons/BackIcon/BackIcon';
import React from 'react';
import { Link } from 'react-router-dom';
import vietnam from '../../../resource/img/vietnam.png'
import Logo from 'app/components/Common/Logo/Logo';
import LockIcon from 'app/components/Icons/LockIcon/LockIcon';
import { useSelector, useDispatch } from 'react-redux';
import { login } from 'store/reducer/authReducer';
import { useForm, SubmitHandler } from "react-hook-form";
import { useHistory } from 'react-router-dom';
type RegisterData = {
  username: string;
  password: string;
  passwordRepeat: string;
}
const LoginPage: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterData>();
  const onSubmit: SubmitHandler<RegisterData> = async (data) => {
    try {
      const repsonse = await dispatch(login(data));
      history.push('/');
    } catch (err) {
      console.log(err);
    }
  }
  return (
    <>
      <div className='w-100 px-3 bg-main py-5'>
        <div className='h-100 d-flex bg-white px-3 rounded-md justify-content-center align-items-center w-100 py-3 flex-column position-relative'>
          <div className="">
            <Logo />
          </div>
          <div className='w-100 mt-3 d-flex flex-column justify-content-center align-items-center text-dark' >
            <h1 className='fw-semibold font-20 mb-0'>Đăng kí tài khoản</h1>
            <p className='text-center font-sm text-grey my-2'>Chào mừng bạn đến với car booking app</p>
          </div>
          <form action="" className="w-100">
            <div>
              <div className="form-heading">
                <p className='font-sm fw-bold mb-2'>Enter your phone</p>
              </div>
              <div className="w-100 form-input-group">
                <div className="flag rounded-circle" style={{ backgroundImage: `url(${vietnam})` }}></div>
                <div className="arrow-down"></div>
                <input
                  {...register('username', {
                    required: {
                      value: true,
                      message: "Số điện thoại không được bỏ trống"
                    },

                  })}
                  autoFocus
                  type="string"
                  placeholder='Nhập số điện thoại'
                  className='phone-input w-100 outline-none'
                />
              </div>
              {errors.username && <span className='text-danger'>{errors.username.message}</span>}
            </div>
            <div className='mt-3'>
              <div className="form-heading">
                <p className='font-sm fw-bold mb-2'>Password</p>
              </div>
              <div className="w-100 form-input-group">
                <div className='input-icon'>
                  <LockIcon size={24} />
                </div>

                <input
                  {...register('password', {
                    required: {
                      value: true,
                      message: "Hãy nhập mật khẩu"
                    },
                  })}
                  type="password"
                  placeholder='Nhập mật khẩu'
                  className='phone-input w-100 outline-none'
                // value=
                />
              </div>
              {errors.password && <span className='text-danger'>{errors.password.message}</span>}
            </div>
            <div className='mt-3'>
              <div className="form-heading">
                <p className='font-sm fw-bold mb-2'>Password</p>
              </div>
              <div className="w-100 form-input-group">
                <div className='input-icon'>
                  <LockIcon size={24} />
                </div>

                <input
                  {...register('passwordRepeat', {
                    required: {
                      value: true,
                      message: "Hãy nhập lại mật khẩu để xác nhận"
                    },
                  })}
                  type="password"
                  placeholder='Nhập lại mật khẩu'
                  className='phone-input w-100 outline-none'
                // value=
                />
              </div>
              {errors.passwordRepeat && <span className='text-danger'>{errors.passwordRepeat.message}</span>}
            </div>
            <div className="form-submit-btn px-5 py-3 font-sm" onClick={handleSubmit(onSubmit)}>
              <span className='m-0'>Continue</span>
            </div>
          </form>
          <div className="to-login w-100 d-flex align-items-center justify-content-center mt-2">
            <p className="text-md">Đã có tài khoản ? Đăng nhập <Link to='/login' className='text-uppercase '>tại đây</Link></p>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginPage;
