import BackIcon from 'app/components/Icons/BackIcon/BackIcon';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import vietnam from '../../../resource/img/vietnam.png'
import Logo from 'app/components/Common/Logo/Logo';
import LockIcon from 'app/components/Icons/LockIcon/LockIcon';
import { useSelector, useDispatch } from 'react-redux';
import { login } from 'store/reducer/authReducer';
import { useForm, SubmitHandler } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { saveRefreshToken, saveToken, saveUser } from 'app/helpers/localStorage';
// import PageHeader from 'app/components/Common/PageHeader/PageHeader';
type LoginData = {
  username: string;
  password: string;
}
const LoginPage: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError, clearErrors
  } = useForm<LoginData>();
  const onSubmit: SubmitHandler<LoginData> = async (data) => {
    try {

      const response: any = await dispatch(login(data));
      console.log(response.payload);
      if (response.payload.error) {
        setError('password', { type: 'manual', message: response.payload.message });
      } else if (response.payload.statusCode < 200 || response.payload.statusCode >= 300) {
        setError('password', { type: 'manual', message: 'Đã có lỗi xảy ra, vui lòng thử lại' })

      } else {
        saveUser({ ...response.payload.user });
        saveToken(response.payload.accessToken);
        saveRefreshToken(response.payload.refreshToken);
        history.push('/');
        console.log('navigate to home page ...');

      }
    } catch (err: any) {
      console.log(err);

    }
  }
  return (
    <>
      <div className='w-100 px-3 bg-main py-5'>
        <div className='h-100 d-flex bg-white px-3 rounded-md justify-content-center align-items-center w-100 py-3 flex-column position-relative'>
          <div className="">
            <Logo />
          </div>
          <div className='w-100 mt-3 d-flex flex-column justify-content-center align-items-center text-dark' >
            <h1 className='fw-semibold font-20 mb-0'>Đăng nhập</h1>
            <p className='text-center font-sm text-grey my-2'>Chào mừng bạn trở lại</p>
          </div>
          <form action="" className="w-100">
            <div>
              <div className="form-heading">
                <p className='font-sm fw-bold mb-2'>Tài khoản (Số điện thoại)</p>
              </div>
              <div className="w-100 form-input-group">
                <div className="flag rounded-circle" style={{ backgroundImage: `url(${vietnam})` }}></div>
                <div className="arrow-down"></div>
                <input
                  {...register('username', {
                    required: {
                      value: true,
                      message: "Số điện thoại không được bỏ trống"
                    },

                  })}
                  autoFocus
                  type="string"
                  placeholder='Nhập số điện thoại'
                  className='phone-input w-100 outline-none'
                />
              </div>
              {errors.username && <span className='text-danger'>{errors.username.message}</span>}
            </div>
            <div className='mt-3'>
              <div className="form-heading">
                <p className='font-sm fw-bold mb-2'>Mật khẩu</p>
              </div>
              <div className="w-100 form-input-group">
                <div className='input-icon'>
                  <LockIcon size={24} />
                </div>

                <input
                  {...register('password', {
                    required: {
                      value: true,
                      message: "Hãy nhập mật khẩu"
                    },
                  })}
                  type="password"
                  placeholder='Nhập mật khẩu'
                  className='phone-input w-100 outline-none'
                // value=
                />
              </div>
              {errors.password && <span className='text-danger'>{errors.password.message}</span>}
            </div>
            <div className="form-submit-btn px-5 py-3 font-sm" onClick={handleSubmit(onSubmit)}>
              <span className='m-0'>Continue</span>
            </div>
          </form>
          <div className="to-login w-100 d-flex align-items-center justify-content-center mt-2">
            <p className="text-md">Chưa có tài khoản ? Đăng kí <Link to='/register' className='text-uppercase '>tại đây</Link></p>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginPage;
