import PageHeader from "app/components/Common/PageHeader/PageHeader";
import React, { useState } from "react";
import { InputDetailItem } from "../BookCarDetailPage/BookCarDetailPage"
import { EyeInvisibleOutlined } from "@ant-design/icons";
import { FormProvider, useForm } from "react-hook-form";
import { IndexedObject } from "types/common";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { changePassword } from "store/reducer/userReducer";
import { isEmptyObject } from "app/helpers/common";

type FormData = {
    passwordOld: string;
    passwordNew: string;
    repeatPassword: string;
}
const ChangePasswordPage = () => {
    const history = useHistory();

    const methods = useForm<FormData>();

    const dispatch = useDispatch();
    const onSubmit = (data: FormData) => {
        console.log("Form data:", data);
        if (isEmptyObject(methods.formState.errors)) {
            dispatch(changePassword(data));
        }
    };

    const password = methods.watch("passwordNew");
    if (methods.getValues("passwordNew")) {
        console.log("New password", methods.getValues("passwordNew").length !== 6);
    }
    console.log();



    return (
        <div className="w-100 page">
            <PageHeader text="Đổi mật khẩu" />
            <div className="w-100 px-3 mt-3">
                <FormProvider {...methods} >
                    <form>

                        <InputDetailItem
                            type="password"
                            label="Mật khẩu cũ"
                            placeHolder="Nhập mật khẩu cũ"
                            name="passwordOld"
                            // register={methods.register}
                            icon={<EyeInvisibleOutlined rev style={{ fontSize: 24 }} />}
                            rules={
                                {
                                    required: "Vui lòng nhập mật khẩu hiện tại",
                                    validate: (value: string) => value.length === 6 ? null : "Mật khẩu chỉ bao gồm 6 kí tự số",
                                }
                            }
                        />
                        <InputDetailItem
                            type="password"
                            label="Mật khẩu mới"
                            placeHolder="Nhập mật khẩu mới"
                            name="passwordNew"
                            rules={
                                {
                                    required: "Vui lòng nhập mật khẩu mới",
                                    validate: (value: string) => value.length === 6 ? null : "Mật khẩu chỉ bao gồm 6 kí tự số",
                                }
                            }
                            icon={<EyeInvisibleOutlined rev style={{ fontSize: 24 }} />}
                        />
                        <InputDetailItem
                            type="password"
                            label="Mật khẩu mới"
                            placeHolder="Nhập lại mật khẩu mới"
                            name="repeatPassword"
                            rules={
                                {
                                    required: "Vui lòng nhập lại mật khẩu mới",
                                    validate: (value: string) => value === password || "Mật khẩu nhập lại không khớp",
                                }
                            }
                            icon={<EyeInvisibleOutlined rev style={{ fontSize: 24 }} />}
                        />
                        <div className="w-100 mt-4 d-flex flex-row justify-content-between">
                            <div
                                onClick={() => {
                                    history.goBack();
                                }}
                                className="w-50 px-3">
                                <div className="bg-white rounded-button shadowed-button px-2 py-2 text-center fw-bold">Hủy bỏ</div>
                            </div>
                            <div className="w-50 px-3">
                                <div
                                    role="button"
                                    onClick={methods.handleSubmit(onSubmit)}
                                    className="w-100 bg-main border-none outline-none rounded-button shadowed-button px-2 py-2 text-center fw-bold text-white"
                                >
                                    Đổi mật khẩu
                                </div>
                            </div>
                        </div>
                    </form>
                </FormProvider>
            </div>

        </div>
    )
}

export default ChangePasswordPage