import { SearchOutlined } from '@ant-design/icons';
import CarItemCard from 'app/components/Common/CarItemCard/CarItemCard';
import PageHeader from 'app/components/Common/PageHeader/PageHeader';
import Tabs, { TabProps } from 'app/components/Common/Tabs/Tabs';
import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { TCar } from 'types/common';
import { useDispatch, useSelector } from 'react-redux';
import { getAllCar, cars, search } from 'store/reducer/carReducer';
// const fakeCarData: Array<TCar> = [
//     {
//         carId: '1',
//         carImage: 'https://cdni.autocarindia.com/utils/imageresizer.ashx?n=https://cms.haymarketindia.net/model/uploads/modelimages/Hyundai-Grand-i10-Nios-200120231541.jpg&w=872&h=578&q=75&c=1  ',
//         carName: 'Huyndai Accent Special Edition',
//         carLicensePlate: '29A-459.72',
//         carStatus: false,
//         carBrand: '',
//     },
//     {
//         carId: '2',
//         carImage: 'https://vinfastotominhdao.vn/wp-content/uploads/ngoai-that-vinfast-president.jpg',
//         carName: 'Huyndai Accent Special Edition',
//         carLicensePlate: '29A-459.72',
//         carStatus: false,
//         carBrand: '',
//     },
//     {
//         carId: '3',
//         carImage: '',
//         carName: 'Huyndai Accent Special Edition',
//         carLicensePlate: '29A-459.72',
//         carStatus: false,
//         carBrand: '',
//     },
//     {
//         carId: '4',
//         carImage: 'https://cdni.autocarindia.com/utils/imageresizer.ashx?n=https://cms.haymarketindia.net/model/uploads/modelimages/Hyundai-Grand-i10-Nios-200120231541.jpg&w=872&h=578&q=75&c=1  ',
//         carName: 'Huyndai Accent Special Edition',
//         carLicensePlate: '29A-459.72',
//         carStatus: false,
//         carBrand: '',
//     },
//     {
//         carId: '5',
//         carImage: 'https://cdni.autocarindia.com/utils/imageresizer.ashx?n=https://cms.haymarketindia.net/model/uploads/modelimages/Hyundai-Grand-i10-Nios-200120231541.jpg&w=872&h=578&q=75&c=1  ',
//         carName: 'Huyndai Accent Special Edition',
//         carLicensePlate: '29A-459.72',
//         carStatus: false,
//         carBrand: '',
//     },

// ]

const BookCarPage = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        const fetchCar = async () => {
            const result: any = await dispatch(getAllCar());
            console.log(result);

            if (result.payload.statusCode < 200 || result.payload.statusCode >= 300) {
                history.push("/login");
            }
        }
        fetchCar();
    }, [])
    const [searchInput, setSearchInput] = useState('')
    const carList = useSelector(cars);
    const tabs: Array<TabProps> = [
        {
            id: 1,
            title: "Sẵn sàng",
            content: carList.filter((car) => {
                return car.vehicle.status_vehicle === "Sẵn sàng"
            }).map((car, index) => {
                return (

                    <CarItemCard
                        key={index}
                        driverName={car.fullname}
                        carLicensePlate={car.vehicle.license_plate}
                        carId={car.vehicle.id_vehicle}
                        carImage={car.vehicle.image_vehicle}
                        carName={car.vehicle.type}
                        carStatus={car.vehicle.status_vehicle}
                        seats={car.vehicle.seats}
                        driverId={car.id_user} />
                )
            }),

        },
        {
            id: 2,
            title: "Đang bận",
            content: carList.filter((car) => {
                return car.vehicle.status_vehicle === "Đang bận"
            }).map((car, index) => {
                return (
                    <>
                        <CarItemCard
                            key={index}
                            driverName={car.fullname}
                            carLicensePlate={car.license_plate}
                            carId={car.vehicle.id_vehicle}
                            carImage={""}
                            carName={car.vehicle.type}
                            carStatus={car.vehicle.status_vehicle}
                            seats={car.vehicle.seats}
                            driverId={car.driverId} />
                    </>
                )
            }),

        },
    ]
    return (
        <div className='w-100 page'>
            <PageHeader text='Đặt xe' />
            <div className="w-100 px-3 mt-3 position-relative">
                <input
                    className="search-car-input border-main outline-none w-100" placeholder='Tìm kiếm'
                    value={searchInput}
                    onChange={(e) => {
                        setSearchInput(e.target.value);
                        dispatch(search(e.target.value))
                    }}
                >

                </input>
                <div className='search-car-input-icon'>
                    <SearchOutlined rev style={{ fontSize: 20 }} />
                </div>
            </div>
            <div className="w-100">
                <Tabs items={tabs} defaultActiveTab={1} />
            </div>
        </div>
    )
}

export default BookCarPage