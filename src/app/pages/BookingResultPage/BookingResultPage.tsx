import React from 'react'
import bookingSuccess from '../../resource/img/booking_success.svg';
import bookingFailed from '../../resource/img/booking_failed.svg';
import { useParams } from 'react-router-dom';
import { IndexedObject } from 'types/common';

const BookingResultPage: React.FC = () => {
    const slug: IndexedObject = useParams();
    console.log(slug.status);

    return (
        <div className='w-100 bg-main px-5 py-5 vh-100'>
            {
                slug.status === 'success'
                    ?
                    <>
                        <div className="w-100 h-100 bg-white px-3 py-3 rounded-md">
                            <div className='w-100 mb-5 d-flex'>
                                <img src={bookingSuccess} alt="" height={200} className='mx-auto' />
                            </div>
                            <div className="d-flex w-100 justify-content-center flex-column ">
                                <p className='text-center mt-4 text-main font-bold font-20 fw-semibold my-3'>Yêu cầu đặt xe thành công</p>
                                <small className='text-grey text-center'>Vui lòng chờ duyệt</small>
                            </div>
                            <div className="buttons w-100 mt-4 d-flex justify-content-center flex-column align-items-center">
                                <a className="bg-main px-3 py-2 rounded-button text-none text-white text-center w-75 my-2 shadowed-button" href='/car-booking'>
                                    Đặt xe khác
                                </a>
                                <a className="bg-white px-3 py-2 rounded-button text-none text-main text-center w-75 my-2 shadowed-button" href='/'>
                                    Về trang chủ
                                </a>
                            </div>
                        </div>
                    </>
                    :
                    <>
                        <div className="w-100 h-100 bg-white px-3 py-3 rounded-md">
                            <div className='w-100 mb-5 d-flex'>
                                <img src={bookingFailed} alt="" height={200} className='mx-auto' />
                            </div>
                            <div className="d-flex w-100 justify-content-center flex-column ">
                                <p className='text-center mt-4 text-danger font-bold font-20 fw-semibold my-3'>Yêu cầu đặt xe thất bại</p>
                                <small className='text-grey text-center'>Đã có lỗi xảy ra trong quá trình đặt xe của bạn</small>
                            </div>
                            <div className="buttons w-100 mt-4 d-flex justify-content-center flex-column align-items-center">
                                <a className="bg-main px-3 py-2 rounded-button text-none text-white text-center w-75 my-2 shadowed-button" href='/car-booking'>
                                    Thử lại
                                </a>
                                <a className="bg-white px-3 py-2 rounded-button text-none text-main text-center w-75 my-2 shadowed-button" href='/'>
                                    Hủy bỏ
                                </a>
                            </div>
                        </div>
                    </>
            }
        </div>
    );
}

export default BookingResultPage