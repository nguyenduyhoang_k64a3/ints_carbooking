import PageHeader from '../../components/Common/PageHeader/PageHeader';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { car, getCarById } from 'store/reducer/carReducer';
import { IndexedObject } from 'types/common';


const CarDetailPage = () => {
    const slug: IndexedObject = useParams()
    console.log(slug);
    const [id, setId] = useState(slug.id);
    const dispatch = useDispatch();
    const carDetail = useSelector(car)
    useEffect(() => {
        dispatch(getCarById(id))
    }, [])


    return (
        <div className='w-100 page' >
            <PageHeader text='Chi tiết về xe' />
            <div className='mt-5 w-100 px-3 '>
                <div className={`px-3 rounded-200 mt-3 text-white text-center status-button-disabled ${carDetail.carStatus === "Sẵn sàng" ? 'bg-main' : 'bg-disabled'}`}>
                    {carDetail.carStatus}
                </div>
                <div className='bg-disabled  px-3 rounded-200 mt-3 text-white text-center status-button-disabled'>
                    {carDetail.carName}
                </div>
            </div>
            <div className='mt-5 w-100 px-3 '>
                <DetailItem title='Biển số xe' content={carDetail.carLicensePlate} />
                <DetailItem title='Số lượng chỗ ngồi' content={`${carDetail.seats}`} />
                <DetailItem title='Họ và tên tài xế' content={carDetail.driverName} />
                <DetailItem title='Phòng ban chuyên môn' content={carDetail.department} />
                <DetailItem title='Số điện thoại tài xế' content={carDetail.driverPhone} />
            </div>
            <div className="mt-3 w-100 px-3 d-flex justify-content-end align-items-center">
                <Link to={'/car-booking/' + id} className="bg-main px-3 rounded-200  text-center py-2  w-50">
                    <span className="mb-0 text-white font-sm" style={{ lineHeight: '22px' }}>Đặt xe</span>
                </Link>
            </div>
        </div>
    )
}

export default CarDetailPage

export const DetailItem: React.FC<{ title: string; content: string; icon?: any }> = ({ title, content, icon }) => {
    return (
        <>
            <div className="w-100 card-detail-item mb-3 rounded-200 position-relative">
                <div className="w-100 d-flex align-items-start justify-content-center flex-column">
                    <span className="mb-1 font-xs text-disabled">{title}</span>
                    <span className="mb-0 text-disabled">{content}</span>
                </div>
                <div className="position-absolute car-detail-item-icon">
                    {icon}
                </div>
            </div>
        </>
    )
}