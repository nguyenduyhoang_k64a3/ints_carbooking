import PageHeader from 'app/components/Common/PageHeader/PageHeader'
import { TabProps } from 'app/components/Common/Tabs/Tabs';
import React, { useEffect } from 'react'
import Tabs from 'app/components/Common/Tabs/Tabs';
import TravelItem, { TravelItemProps } from 'app/components/Common/TravelItem/TravelItem';
import CompletedTravel from 'app/components/Common/CompletedTravel/CompletedTravel';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RootState } from 'types/RootState';
import { getCompletedTravel, getIncomingTravel } from 'store/reducer/travelReducer';

const TravelHistoryPage: React.FC = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const incomingTravel = useSelector((state: RootState) => state.travel.incomingTravel);
    const completedTravel = useSelector((state: RootState) => state.travel.completedTravel);
    useEffect(() => {
        const fetchIncomingTravel = async () => {
            const res = await dispatch(getIncomingTravel());
            console.log(res);

        }
        fetchIncomingTravel();
        const fetchCompletedTravel = async () => {
            const res = await dispatch(getCompletedTravel());
            console.log('fetchCompletedTravel', res);
        }
        fetchCompletedTravel();
    }, [])

    const tabs: Array<TabProps> = [
        {
            id: 1,
            title: "Chuyến đi sắp tới",
            content: <>
                {
                    incomingTravel.length > 0 ?
                        incomingTravel.map((item, index) => {
                            return <TravelItem
                                key={index}
                                carImage={item.driver?.vehicle.image_vehicle}
                                timeStart={`${new Date(item.date_start).toLocaleDateString()} ${item.time_start}`}
                                timeEnd={`${new Date(item.date_end).toLocaleDateString()} ${item.time_end}`}
                                travelId={item.id_booking}
                            />
                        })
                        :
                        <p className='px-3'>Không tìm thấy chuyến đi sắp tới</p>
                }
            </>,

        },
        {
            id: 2,
            title: "Chuyến đã đi",
            content: <>
                {
                    completedTravel.length > 0 ?
                        completedTravel.map((item, index) => {
                            return <TravelItem
                                key={index}
                                carImage={item.driver?.vehicle.image_vehicle}
                                timeStart={`${new Date(item.date_start).toLocaleDateString()} ${item.time_start}`}
                                timeEnd={`${new Date(item.date_end).toLocaleDateString()} ${item.time_end}`}
                                travelId={item.id_booking}
                            />
                        })
                        :
                        <p className='px-3'>Không tìm thấy chuyến đã đi</p>
                }
            </>,

        },
    ]

    return (
        <div className='w-100 page'>
            <PageHeader text='Lịch sử chuyến đi' />
            <div className="w-100">
                <Tabs
                    defaultActiveTab={1}
                    items={tabs}
                />
            </div>
        </div>
    )
}

export default TravelHistoryPage